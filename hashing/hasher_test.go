// Copyright 2023 The Accumulate Authors
//
// Use of this source code is governed by an MIT-style
// license that can be found in the LICENSE file or at
// https://opensource.org/licenses/MIT.
package hashing_test

import (
	"crypto/sha256"
	"fmt"
	"testing"
	"time"

	. "github.com/pegnet/LXRMining/hashing"
	"github.com/pegnet/LXRMining/pow"
)

func Test_Hasher(t *testing.T) {
	lx := new(pow.LxrPow)
	lx.Init(32, 30, 6)
	m := NewHasher(1, 1000, lx)
	m.Start()
	hash := sha256.Sum256([]byte{1, 2, 3, 4})
	difficulty := uint64(0xFF000000000000)
	m.BlockHashes <- Hash{DNHash: hash, Difficulty: difficulty}
	start := time.Now()
	for i := 0; i < 40; i++ {
		s := <-m.Solutions
		fmt.Printf("%08x %016x %016x %016d\n", s.DNHash[:8], s.Nonce, s.PoW, s.HashCnt)
		if s.PoW > difficulty {
			hash = sha256.Sum256(hash[:])
			m.BlockHashes <- Hash{DNHash: hash, Difficulty: difficulty}
			fmt.Println()
			hs := float64(time.Since(start).Milliseconds()) / 1000
			fmt.Printf("Hashes Per Second = %8.3f\n", float64(s.HashCnt)/float64(hs))

		}
	}
}

func Test_Float(t *testing.T) {
	t.Skip()
	cu := uint64(0xffffffffffffffff)
	cs := int64(cu)
	fmt.Printf("                        cu = %x\n", cu)
	fmt.Printf("                    int cu = %d\n", cs)
	fmt.Printf("                  float cu = %f\n", float64(cu))
	fmt.Printf("         float of cs *1000 = %f\n", float64(cs*1000))
	fmt.Printf("uint of (float of cs*1000) = %x\n", uint64(float64(cs*1000)))
}
