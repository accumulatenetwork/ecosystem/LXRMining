// Copyright 2023 The Accumulate Authors
//
// Use of this source code is governed by an MIT-style
// license that can be found in the LICENSE file or at
// https://opensource.org/licenses/MIT.
package hashing

import (
	"github.com/pegnet/LXRMining/pow"
)

// All that is needed to create a Hasher instance.  Once it is created,
// It will run and feed back solutions
type HasherSet struct {
	Instances   []*Hasher
	BlockHashes chan Hash
	Solutions   chan PoWSolution
	HSolutions  chan PoWSolution
	Control     chan string
	Nonce       uint64
	Lx          *pow.LxrPow
	Started     bool
}

// NewHashers
// Create a set of Hashers to utilize the available cores on a computer
// system.  A Solutions Channel is used by all the go routines to report
// their best results.
func NewHashers(Instances, Nonce uint64, Lx *pow.LxrPow) *HasherSet {
	h := new(HasherSet)
	h.Nonce = Nonce
	h.Lx = Lx
	h.BlockHashes = make(chan Hash, 100)
	h.Solutions = make(chan PoWSolution, 1000)
	h.HSolutions = make(chan PoWSolution, 1000)
	h.Control = make(chan string, 10)

	for i := uint64(0); i < Instances; i++ {
		n := h.Nonce ^ uint64(i)
		n = n<<19 ^ n>>11

		instance := NewHasher(i, n, Lx)
		instance.Solutions = h.HSolutions           // override Solutions channel
		h.Instances = append(h.Instances, instance) // Collect all our instances

		instance.Start()
	}

	return h
}

// SetSolutions
// Direct solutions to the given solutions channel
func (h *HasherSet) SetSolutions(solutions chan PoWSolution) {
	h.Solutions = solutions
}

func (h *HasherSet) Stop() {
	if !h.Started {
		return
	}
	h.Control <- "stop"
	h.Started = false
	for _, i := range h.Instances {
		i.Stop()
	}
}

func (h *HasherSet) Start() {
	if h.Started {
		return
	}
	h.Started = true
	go func() {
		for {
			select {
			case hash := <-h.BlockHashes:
				for len(h.Solutions) > 0 { // Toss any pending solutions
					<-h.Solutions
				}
				for _, i := range h.Instances {
					i.BlockHashes <- hash
				}
			case <-h.Control:
				return
			case s := <-h.HSolutions:
				h.Solutions <- s
			}
		}
	}()
}
