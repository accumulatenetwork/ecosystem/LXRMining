// Copyright 2023 The Accumulate Authors
//
// Use of this source code is governed by an MIT-style
// license that can be found in the LICENSE file or at
// https://opensource.org/licenses/MIT.
package hashing

import (
	"crypto/sha256"
	"fmt"
	"testing"
	"time"

	"github.com/pegnet/LXRMining/pow"
)

func Test_HasherSet(t *testing.T) {
	lx := new(pow.LxrPow)
	lx.Init(32, 30, 6)
	m := NewHashers(7, 523452345, lx)
	m.Start()
	hash := sha256.Sum256([]byte{1, 2, 3, 4})
	difficulty := uint64(0xffff000000000000)
	fmt.Print("\nStart Hashing\n\n")
	m.BlockHashes <- Hash{DNHash: hash, Difficulty: difficulty}

	var best PoWSolution
	for i := 0; i < 1; {
		s := <-m.Solutions
		if s.PoW > best.PoW || s.PoW > 0xffff000000000000 {
			var th uint64
			for _, v := range m.Instances {
				th += v.HashCnt
			}
			if best.PoW == 0 {
				fmt.Println()
				fmt.Printf("%6s%v%12s%v%12s%v%13s%v\n", "", "Hash", "", "Nonce", "", "PoW", "", "Hasher")
			}
			best = s
			fmt.Printf("%08x %016x %016x %10d\n", s.DNHash[:8], s.Nonce, s.PoW, th)
			if s.PoW > 0xFFFF000000000000 {
				best.PoW = 0
				hash = sha256.Sum256(hash[:])
				m.BlockHashes <- Hash{DNHash: hash, Difficulty: difficulty}
				i++
			}
		}
	}
	m.Stop()
	time.Sleep(time.Second)
}
