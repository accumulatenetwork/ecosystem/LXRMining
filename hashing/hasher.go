// Copyright 2023 The Accumulate Authors
//
// Use of this source code is governed by an MIT-style
// license that can be found in the LICENSE file or at
// https://opensource.org/licenses/MIT.
package hashing

import (
	"time"

	"github.com/pegnet/LXRMining/pow"
)

type PoWSolution struct {
	Block    uint64    // Block Number
	TokenURL string    // Payout token account
	Instance int16     // ID of the hasher producing this block
	time     time.Time // Timestamp when block was produced
	DNHash   [32]byte  // The hash of the block
	Nonce    uint64    // Nonce that is the solution
	PoW      uint64    // Self-reported Difficulty
	HashCnt  uint64    // Count of hashes performed so far
}

type Hash struct {
	DNHash     [32]byte
	Difficulty uint64
}

// All that is needed to create a Hasher instance.  Once it is created,
// It will run and feed back solutions
type Hasher struct {
	Instance    uint64
	CurrentHash [32]byte
	Nonce       uint64
	BlockHashes chan Hash
	Solutions   chan PoWSolution
	Control     chan string
	Best        uint64
	Started     bool
	HashCnt     uint64
	LX          *pow.LxrPow
}

func NewHasher(instance, nonce uint64, lx *pow.LxrPow) *Hasher {
	m := new(Hasher)
	m.Instance = instance
	m.Nonce = nonce
	m.LX = lx

	// Inputs to the Hasher
	m.Control = make(chan string, 10)     // The Hasher stops when told on this channel
	m.BlockHashes = make(chan Hash, 1000) // Hashes to Hash are read from this channel

	// Outputs from the Hasher (can be overwritten and thus shared across Hashers)
	m.Solutions = make(chan PoWSolution, 1000) // Solutions are written to this channel

	return m
}

func (m *Hasher) Stop() {
	if !m.Started {
		return
	}
	m.Control <- "stop"
	m.Started = false
}

func (m *Hasher) Start() {
	if m.Started {
		return
	}

	m.Started = true
	go func() {
		var difficulty uint64
		hash := <-m.BlockHashes
		for {
			time.Sleep(time.Microsecond)
			select {
			case hash = <-m.BlockHashes:
				m.CurrentHash = hash.DNHash
				difficulty = hash.Difficulty
			case <-m.Control:
				return
			default:
			}
			for i := 0; i < 1000; i++ {
				m.HashCnt++
				m.Nonce ^= m.Nonce<<17 ^ m.Nonce>>9 ^ m.HashCnt // diff nonce for each instance
				nPoW := m.LX.LxrPoW(hash.DNHash[:], m.Nonce)
				if nPoW > difficulty {
					m.Solutions <- PoWSolution{
						Block:    0,
						TokenURL: "",
						Instance: int16(m.Instance),
						time:     time.Now(),
						DNHash:   hash.DNHash,
						Nonce:    m.Nonce,
						PoW:      nPoW,
						HashCnt:  m.HashCnt,
					}
					break
				}
			}
		}
	}()
}
