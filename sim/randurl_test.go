// Copyright 2023 The Accumulate Authors
//
// Use of this source code is governed by an MIT-style
// license that can be found in the LICENSE file or at
// https://opensource.org/licenses/MIT.
package sim

import (
	"fmt"
	"testing"
)

func TestURL(t *testing.T) {
	dups := 0
	all := make(map[string]int)
	for i := 0; i < 20000; i++ {
		a := GetURL()
		if v, ok := all[a]; ok {
			dups++
			fmt.Printf("%5d Duplicates: %s %d %d\n", dups, a, v, i)
		}
		all[a] = i
		if i < 100 || i%100000 == 0 {
			fmt.Println(i, " ", a)
		}
	}
}
