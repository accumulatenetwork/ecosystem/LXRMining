// Copyright 2023 The Accumulate Authors
//
// Use of this source code is governed by an MIT-style
// license that can be found in the LICENSE file or at
// https://opensource.org/licenses/MIT.

unsigned long long const Size = 0x40000000;             // Size of the BitMap.  Should be 1 GB (0x40000000) for production
int const Passes = 6;                                   //
int const Loops = 16;                                   // How many times the hash is looped
unsigned long long const checksum = 0xed0536be7833ca15; // Checksum of byteMap to ensure we don't use a corrupted file

// Lookup XoR Hash (Ram Hash)
class LXRHash
{
    unsigned char byteMap[Size];                                                 // 1 GB is 30 bits, 2^30, or hex 4 with 7 zeros
    unsigned long long MapMask = Size - 1;                                       // MapMask is byteMap length -1
public:                                                                          //
    unsigned long long mix(char unsigned hash[32], unsigned long long nonce);    // Mixes the hash with the nonce prior to
    unsigned long long LxrPoW(char unsigned hash[32], unsigned long long nonce); // Returns the PoW for a hash + nonce
    void init();                                                                 // Init the byteMap; do it only once.
private:                                                                         //
    bool load();                                                                 // Load the byteMap.dat file
    unsigned long long check();                                                  // Quick check of correct byteMap
};
