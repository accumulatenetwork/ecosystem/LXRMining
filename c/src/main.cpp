// Copyright 2023 The Accumulate Authors
//
// Use of this source code is governed by an MIT-style
// license that can be found in the LICENSE file or at
// https://opensource.org/licenses/MIT.

#include <iostream>
#include <fstream>
#include <cstdio>
#include <chrono>
#include <ctime>
#include "../include/lxrhash.h"

LXRHash lxrHash;

int main()
{
    printf("Load byteMap\n");
    lxrHash.init();                                                   // init() loads/computes byteMap
    printf("The byteMap is loaded, mining begins!\n");                //
    printf("%13s %13s %10s %16s   %15s\n",                            //
           "total(s)", "block(s)", "nonce", "PoW", "hashes/sec");     //
    unsigned char hash[32];                                           // Start with a hash of all zeros
    std::fill_n(hash, 32, 0);                                         //
    unsigned long long hashCnt = 0;                                   // Count hashes to compute hash/second (hps)
    unsigned long long ms = 0;                                        // Set the current time passed to zero
    for (int i = 0; i < 1000; i++)                                    //
    {                                                                 //
        printf("Now Mining: ");                                       // Start mining the new hash
        for (int i = 0; i < 32; i++)                                  // Print the new hash in hex
        {                                                             //
            printf("%02x", hash[i]);                                  //
        }                                                             //
        printf("\n");                                                 //
        auto last = std::chrono::system_clock::now();                 // time
        unsigned long long ms1 = 0;                                   // block time
        unsigned long long min = 0xFFFFFFFFFFFFFFFF;                  // set min to max unsigned long long value
        bool done = false;                                            // Done is set when a block difficulty is found
        for (unsigned long long nonce = 0; !done; nonce++, hashCnt++) // Loop until done, count nonces and hashes
        {                                                             //
            unsigned char tmpHash[32];                                // Copy hash since LxrPoW will overwrite
            std::copy(hash, hash + 32, tmpHash);                      //
            unsigned long long v = lxrHash.LxrPoW(tmpHash, nonce);    // Get the LxrPow for current nonce
            if (v < min && v < (unsigned long long)(1) << 52)         // Print new solutions < some minimum difficulty
            {                                                         //
                min = v;                                              // Set as the new highest solution found
                auto end = std::chrono::system_clock::now();          // Calculate time stamps
                unsigned long long ms2 =                              //
                    std::chrono::duration_cast<std::chrono::milliseconds>(end - last).count();
                ms1 += ms2;                                        // Add to the time in this block
                ms += ms2;                                         // Add to total time spent
                last = std::chrono::system_clock::now();           // Reset timer for this block
                printf("%13.3f %13.3f %10llu %016llx   %15.3f \n", // Print out the new solution
                       float(ms) / 1000,                           // Total time
                       float(ms1) / 1000,                          // Time mining this block
                       nonce,                                      // The nonce solution found
                       v,                                          // The mining value using the nonce
                       float(hashCnt) / (float(ms) / 1000 + 1));   // Total hashes per second in this test
            }                                                      //
            done = v < (unsigned long long)(1) << 45;              // Check against the difficulty for EOB
            if (done)                                              // If at EOB
            {                                                      //   then set up a new block to mine
                std::copy(tmpHash, tmpHash + 32, hash);            // Use the tmpHash as a new hash!
            }
        }
    }
    return 0;
}
