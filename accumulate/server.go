// Copyright 2023 The Accumulate Authors
//
// Use of this source code is governed by an MIT-style
// license that can be found in the LICENSE file or at
// https://opensource.org/licenses/MIT.
package accumulate

import (
	"crypto/sha256"
	"fmt"
	"net/url"
	"sort"
	"strings"
	"sync"
	"time"

	"github.com/pegnet/LXRMining/pow"
)

// Structures we are reading and writing to data accounts in Accumulate
// ====================================================================
//
// Submission
// Miners create submissions that are recorded on
// acc://miningService/submissions scratch data account
//
// Miner
// Miners register their token accounts on
// acc://miningService/miners data account.  If a token account
// is registered multiple times, then the first registration is the index
// used to track points
//
// Points
// Every payout (once a day) reports the current point balances of all miners on
// acc://miningService/points.
// To compute the points of a miner, the validators run through all the records on
// this account to add up the points for all the miners.  The
//
// PointsReport
// A structure written out with every payout period of the current point balance of
// every miner whose balance changed during the payout period.  The balances are those
// after the payout is processed.
//
// Settings
// All the assumed settings and state of the Mining process.  This includes information
// about the block completed, the hash to be mined in the next block, parameters to the
// LXRPow, etc.

// MAdi
// This struct represents the state kept in the mining ADI
// We don't persist this; it is where we put stuff we pull
// from accumulate.

type MAdi struct {
	Mutex         sync.RWMutex       // Mutex for updating MAdi
	Submissions   []Submission       // Submissions from Miners
	Accepted      []Submission       // The Accepted winning entry for each block
	Reports       []Report           // Settings needed by miners to mine
	URLRegistry   map[string]uint64  // A look up table to make finding a miner's index fast
	UrlIdx        []string           // List of URLs as they have been registered
	Miners        []string           // List of miners as they have been registered
	Validators    []string           // List of current validators (entries in validator key book)
	ValidatorIdx  map[string]int     // Index of a Validator in the validator key book
	PointsReport  []PointsReport     // Reports of points earned by miners
	Totals        map[uint64]float64 // Totals of token distributions
	LX            *pow.LxrPow        // Not persisted.
	listeners     []chan Report      // Nodes registered to get settings on changes
	blockHashInfo struct {           // Create the state of the next blockHash
		timestamp time.Time //              (used in simulation)
		DNIndex   uint64
		DNHash    [32]byte
	}
}

var MiningADI = new(MAdi)

// publishReport
// Send the Report to all the miners listening for reports from the validators
func (m *MAdi) publishReport(report Report) {
	for _, listener := range m.listeners {
		listener <- report
	}
}

// addValidator
// Takes a validator and adds it to the Mining KeyBook Page
// This is just for simulation.  Management of the Mining KeyBook will be managed by the
// Validators
// Locking is done by the calling routine
func (m *MAdi) addValidator(URL string) error {
	URL = strings.ToLower(URL)             //          URLs have to be lowercased
	if _, ok := m.ValidatorIdx[URL]; !ok { //          If the URL is not already registered, register it
		if _, err := m.url2Index(URL); err != nil { // Getting the index does all the checks on the URL
			return err //                              Error means URL is bad, so report
		}
		m.ValidatorIdx[URL] = len(m.Validators)  // Then register it. Grab its index
		m.Validators = append(m.Validators, URL) // Add the index to validator list
	}
	return nil
}

// NumberValidators
// Return the number of validators
func (m *MAdi) NumberValidators() uint64 {
	m.Mutex.Lock()
	defer m.Mutex.Unlock()
	return uint64(len(m.Validators))
}

// GetValidator
// Return the validator URL by index
func (m *MAdi) GetValidator(idx uint64) string {
	m.Mutex.Lock()
	defer m.Mutex.Unlock()
	if int(idx) < len(m.Validators) {
		return m.Validators[idx]
	}
	return ""
}

// NextDNHash
// In the real implementation, we get this stuff from Accumulate
func (m *MAdi) NextDNHash() (timeStamp time.Time, DNIndex uint64, DNHash []byte) {
	if m.blockHashInfo.timestamp.UnixNano() == 0 {
		m.makeNextDNHash()
	}
	dn := append([]byte{}, m.blockHashInfo.DNHash[:]...)
	return m.blockHashInfo.timestamp, m.blockHashInfo.DNIndex, dn
}

// MakeNextDNHash
// Make a new DNHash
func (m *MAdi) makeNextDNHash() {
	m.blockHashInfo.DNHash = sha256.Sum256(m.blockHashInfo.DNHash[:])
	m.blockHashInfo.timestamp = time.Now()
	m.blockHashInfo.DNIndex++
}

// init
// Set up the miner ADI (MAdi) while we are not actually running against accumulate.
// This needs to be replaced by a call to get the settings from the minder ADI.
// We will still need to create the Miners Map, and parse through the MinerIdx to
// create the Miners Map, compute the current point balances, etc.
func init() {

	MiningADI.URLRegistry = make(map[string]uint64)
	MiningADI.Totals = make(map[uint64]float64)
	MiningADI.ValidatorIdx = make(map[string]int)

	// In development and standalone testing, we need a settings record
	// This will be done using the CLI for Accumulate
	settings := new(Settings)                              // Create a Settings record
	settings.BlockHeight = 0                               // Initial block height is zero
	settings.TimeStamp = time.Now()                        // Current time
	settings.DNIndex = 10000                               // Just pick an arbitrary minor block height
	settings.SubmissionHeight = 1                          // Height of the first submission
	settings.Loops = 16                                    // LXPow: number of loops over the hash
	settings.Bits = 30                                     // LXPow: Number of bits in the Byte Map lookup
	settings.DNHash = sha256.Sum256([]byte("first block")) // Hash of the first DNBlock to be mined
	settings.Difficulty = 0xFF00000000000000               // A Starting difficulty target
	settings.BlockTime = 600                               // 10 minutes default
	settings.PayoutFreq = 25                               // Payout every so many blocks
	settings.Qualifies = 300                               // How many submissions get points
	settings.Budget = 50000                                // Tokens distributed per pay period

	Report := new(Report)
	Report.Settings = *settings
	MiningADI.AddReport(*Report)
	MiningADI.makeNextDNHash()
}

// AddListener
// Listeners will get updates on settings when they occur
// In an Accumulate integration this may need to be implemented by polling
func (m *MAdi) AddListener(listener chan Report) {
	m.Mutex.Lock()
	defer m.Mutex.Unlock()
	m.listeners = append(m.listeners, listener)
}

// SubmissionHt
// Return the height of the submission list.  The settings for a block
// specify the height of the submissions prior to mining a block.
func (m *MAdi) SubmissionHt() uint64 {
	m.Mutex.Lock()
	defer m.Mutex.Unlock()
	return uint64(len(m.Submissions))
}

func (m *MAdi) Url2Index(tokenUrl string) (uint64, error) {
	m.Mutex.Lock()
	defer m.Mutex.Unlock()

	return m.url2Index(tokenUrl)
}

// Url2Index
// Convert a URL to an Index (to save space in records).  If the
// Url is not already registered, then add it to the URL list and
// return that index.  The URL must be valid.
func (m *MAdi) url2Index(tokenUrl string) (uint64, error) {
	if _, err := url.Parse(tokenUrl); err != nil {
		return 0, err
	}

	tokenUrl = strings.ToLower(tokenUrl)
	if idx, registered := m.URLRegistry[tokenUrl]; registered { // Check Registry
		return idx, nil //                                          Return index if we already have one.
	}

	idx := uint64(len(m.UrlIdx))          // Index of the end of list will be the index
	m.UrlIdx = append(m.UrlIdx, tokenUrl) // Add url to the list of URLs
	m.URLRegistry[tokenUrl] = idx         // Register the url in the map
	//TODO: write the UrlIdx list to Accumulate

	return idx, nil
}

// Index2Url
// Locking
// Takes the miner index and returns the miner's URL
func (m *MAdi) Index2Url(minerIdx uint64) string {
	m.Mutex.Lock()
	defer m.Mutex.Unlock()

	if int(minerIdx) >= len(m.UrlIdx) {
		return ""
	}
	return m.UrlIdx[minerIdx]
}

// GetMinerUrl
// No locking
// Takes the miner index and returns the miner's URL
func (m *MAdi) index2Url(minerIdx uint64) string {
	if int(minerIdx) >= len(m.UrlIdx) {
		return ""
	}
	return m.UrlIdx[minerIdx]
}

// RegisterValidator
// Adds a Validator to the Validator data account, and updates the
// Map for easy lookups
func (m *MAdi) RegisterValidator(bookUrl string) (validatorIdx uint64, err error) {
	m.Mutex.Lock()
	defer m.Mutex.Unlock()

	rIdx, err := m.url2Index(bookUrl)
	if err != nil {
		return 0, err
	}
	m.addValidator(bookUrl)

	return rIdx, nil
}

// AddReport
// Add a Settings Record to the Settings Account
func (m *MAdi) AddReport(report Report) {
	m.Mutex.Lock()
	defer m.Mutex.Unlock()
	m.Reports = append(m.Reports, report)
	m.publishReport(report)
	cnt = 0
}

// GetSetting
// Return the Setting by its index and nil, or the first Setting and an error if out of range
func (m *MAdi) GetSetting(i int) (Report, error) {
	m.Mutex.Lock()
	defer m.Mutex.Unlock()
	return m.getReport(i)
}

// getSetting
// Return the Setting by its index and nil, or the first Setting and an error if out of range
func (m *MAdi) getReport(i int) (s Report, err error) {
	if i < 0 || i >= len(m.Reports) {
		return m.Reports[0], fmt.Errorf("the %d setting does not exist", i)
	}

	return m.Reports[i], nil

}

// Sync
// Syncs with the Accumulate Protocol so we can trust our data.
// If a nil is returned, then Syncing failed.
func (m *MAdi) Sync() Report {
	m.Mutex.Lock()
	defer m.Mutex.Unlock()
	return m.sync()
}
func (m *MAdi) sync() Report {
	r, err := m.getReport(len(m.Reports) - 1)
	if err != nil {
		panic(err)
	}
	return r
}

// GetSubmissions()
// Return all submissions from a particular index matching the given hash
// Index returned is the submission after the last submission matching the hash.
// If no submissions are found that match the hash, or no submissions have yet been submitted
// for the index, then the index provided and a nil are returned
func (m *MAdi) GetSubmissions(settings *Settings, index uint64) (submissions []Submission, nextIndex uint64) {
	m.Mutex.Lock()         // Locking and unlocking the MAdi
	defer m.Mutex.Unlock() //

	if index >= uint64(len(m.Submissions)) { // If index out of bounds, just punt
		return nil, index
	}

	nextIndex = index                           // Default nextIndex to nothing found
	for i, sub := range m.Submissions[index:] { //

		if sub.DNHash != settings.DNHash { //      If the hash doesn't match, continue search
			continue
		}

		if validateSubmission(m.LX, *settings, sub) { //
			submissions = append(submissions, sub) //   Add the validated submission to the return list
			nextIndex = index + uint64(i) + 1      //   Remember the index of the following entry
		}
	}
	return submissions, nextIndex
}

var cnt int
var last time.Time = time.Now()

// AddSubmission
// Does quality checks on the submission to avoid adding submissions
// that cannot win points and wasting credits.
func (m *MAdi) AddSubmission(sub Submission) error {
	m.Mutex.Lock()
	defer m.Mutex.Unlock()
	settings := m.sync().Settings
	if validateSubmission(m.LX, settings, sub) {
		fmt.Printf("[%4.1f]",time.Since(last).Seconds())
		last = time.Now()
		m.Submissions = append(m.Submissions, sub) // Everything fits.  Add a submission
		m.makeNextDNHash()                         // This block is done, so go to next block
	} else {
		if cnt%10000 == 0 && cnt > 0 {
			fmt.Println(". ", cnt)
		}
		cnt++
		return fmt.Errorf("bad entry")
	}
	return nil
}

// ValidateSubmission
// Test everything about a submission is good, so the submission can be accepted,
// and we can begin mining the next block
func validateSubmission(LX *pow.LxrPow, settings Settings, submission Submission) bool {
	switch {
	case settings.SubmissionHeight != submission.SubmissionHeight:
		return false
	case settings.DNHash != submission.DNHash:
		return false
	case submission.DNIndex != settings.DNIndex:
		return false
	case MiningADI.index2Url(submission.MinerIdx) == "": // This test must not lock and unlock
		return false
	case LX != nil && LX.LxrPoW(submission.DNHash[:], submission.Nonce) != submission.PoW:
		return false
	}

	return true
}

// PayMiners
// Generate a transaction to pay miners
func (m *MAdi) PayMiners(outputs []TxOutput) {
	sort.Slice(outputs, func(i, j int) bool {
		return outputs[i].Amount > outputs[j].Amount
	})
	fmt.Println()
	fmt.Println("PayOuts")
	for i, o := range outputs {
		if i%16 == 0 {
			fmt.Println()
		}

		m.Totals[o.MinerIndex] += o.Amount
		fmt.Printf("%4d < %5.1f", o.MinerIndex, float64(o.Amount)/1e8)
	}
	fmt.Println()

	fmt.Println("Totals Paid:")
	var totals []TxOutput
	for m, a := range m.Totals {
		tx := TxOutput{}
		tx.MinerIndex = m
		tx.Amount = a
		totals = append(totals, tx)
	}
	sort.Slice(totals, func(i, j int) bool {
		return totals[i].Amount > totals[j].Amount
	})

	for i, p := range totals {
		if i%8 == 0 {
			fmt.Println()
		}
		fmt.Printf("[%4d] %3d = %10.3f | ", i, p.MinerIndex, p.Amount)
	}
	fmt.Println()
}
