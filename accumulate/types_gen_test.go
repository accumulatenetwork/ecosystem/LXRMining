// Copyright 2023 The Accumulate Authors
//
// Use of this source code is governed by an MIT-style
// license that can be found in the LICENSE file or at
// https://opensource.org/licenses/MIT.
package accumulate

import (
	"crypto/sha256"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func Test_Types(t *testing.T) {
	s := new(Submission)
	for i := uint64(0); i < 100; i++ {
		s.Valid = i&1 == 1
		s.TimeStamp = time.Unix(time.Now().Unix(), 0)
		s.DNIndex = i * 5
		s.DNHash = sha256.Sum256([]byte{byte(i), byte(i * 7)})
		s.SubmissionHeight = i * 27
		s.Nonce = i>>23 ^ i<<3
		s.MinerIdx = i * 13
		s.PoW = i>>1 ^ i>>3 ^ i>>7 ^ i>>13

		b, e := s.MarshalBinary()
		assert.NoError(t, e, "should marshal")
		s2 := new(Submission)
		e = s2.UnmarshalBinary(b)
		assert.NoError(t, e, "should unmarshal")
		assert.True(t, s.Equal(s2), "should get same struct")
	}
}
