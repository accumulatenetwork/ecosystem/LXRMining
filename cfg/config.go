// Copyright (c) of parts are held by the various contributors
// Licensed under the MIT License. See LICENSE file in the project root for full license information.
package cfg

import (
	"flag"
	"fmt"
	"os"
	"time"

	"github.com/pegnet/LXRMining/accumulate"
	"github.com/pegnet/LXRMining/pow"
	"github.com/pegnet/LXRMining/sim"
)

type Config struct {
	TokenURL     string      // The Token URL for a miner
	Instances    uint64      // How many hashers to run
	MinerCnt     uint64      // Number of miners to run
	ValidatorCnt uint64      // Number of validators to create
	Difficulty   uint64      // The difficulty limit (if using difficulty to end mining blocks)
	DiffWindow   uint64      // Determines Difficulty adjustments, when ending blocks with difficulty
	BlockTime    uint64      // Used when ending blocks with time (uniform blocks)
	LX           *pow.LxrPow // The Proof of work function to be used.
	PayoutFreq   uint64      // Payouts are made every PayoutFreq blocks.
}

// Return a shallow copy of the configuration settings.
func (c Config) Clone() *Config {
	return &c
}

// Init
// What
func (c *Config) Init() {
	args := os.Args
	_ = args
	pInstances := flag.Uint64("instances", 1, "Number of instances of the hash miners")
	pMinerCnt := flag.Uint64("minercnt", 25, "Number of miners (with random URLs) to run")
	pDifficulty := flag.Uint64("difficulty", 0xfff<<48, "Difficulty target (timed) or difficulty termination (not timed)")
	pDiffWindow := flag.Uint64("diffwindow", 10, "Difficulty Target Valuation in blocks")
	pBlockTime := flag.Uint64("blocktime", 60, "Block Time in seconds (600 would be 10 minutes)")
	pPayoutFreq := flag.Uint64("payoutfreq", 25, "Number of blocks between payouts")
	pValidatorCnt := flag.Uint64("validatorcnt", 5, "Number of validators to create")
	flag.Parse()

	c.Instances = *pInstances
	c.MinerCnt = *pMinerCnt
	c.Difficulty = *pDifficulty
	c.DiffWindow = *pDiffWindow
	c.BlockTime = *pBlockTime
	c.PayoutFreq = *pPayoutFreq
	c.ValidatorCnt = *pValidatorCnt

	if c.MinerCnt < 1 { // The least number of miners we will run is 1
		c.MinerCnt = 1
	}

	c.TokenURL = sim.GetURL()

	fmt.Printf("\nminer --instances=%d --minercnt=%d --difficulty=0x%x --diffwindow=%d --blocktime=%d \n\n",
		c.Instances, c.MinerCnt, c.Difficulty, c.DiffWindow, c.BlockTime,
	)

	c.LX = pow.NewLxrPow(16, 30, 6)
	accumulate.MiningADI.LX = c.LX
	report := &accumulate.MiningADI.Reports[0] // Get the first report and modify it for simulation
	settings := &report.Settings               // Get the settings from the report
	settings.TimeStamp = time.Now()            //
	settings.BlockTime = c.BlockTime           //
	settings.PayoutFreq = c.PayoutFreq         //
	settings.Difficulty = c.Difficulty         //
	settings.DiffWindow = c.DiffWindow         //
	settings.WindowTimestamp = time.Now()      //
	settings.WindowBlockIndex = 1              //

}

func NewConfig() *Config {
	c := new(Config)
	c.Init()
	return c
}
