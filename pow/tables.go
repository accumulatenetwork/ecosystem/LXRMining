// Copyright 2023 The Accumulate Authors
//
// Use of this source code is governed by an MIT-style
// license that can be found in the LICENSE file or at
// https://opensource.org/licenses/MIT.
package pow

import (
	"bufio"
	"encoding/binary"
	"fmt"
	"math"
	"os"
	"os/user"
	"time"
)

// Init initializes the hash with the given values
//
// We use our own algorithm for initializing the map struct.  This is an fairly large table of
// byte values we use to map bytes to other byte values to enhance the avalanche nature of the hash
// as well as increase the memory footprint of the hash.
//
// Bits is the number of bits used to address the ByteMap. If less than 8, set to 8.
// Passes is the number of shuffles of the ByteMap performed.  Each pass shuffles all byte values in the map
func (lx *LxrPow) Init(Loops, Bits, Passes uint64) *LxrPow {
	if Bits < 8 {
		Bits = 8
	}
	if Bits > 32 {
		panic("we do not allow Byte Maps greater than 4 GB in size at this time.")
	}
	lx.MapBits = Bits
	lx.MapSize = uint64(math.Pow(2, float64(Bits)))
	lx.Passes = Passes
	lx.Loops = Loops
	lx.ReadTable()
	return lx
}

// ReadTable attempts to load the ByteMap from disk.
// If that doesn't exist, a new one will be generated and saved.
func (lx *LxrPow) ReadTable() {
	u, err := user.Current()
	if err != nil {
		panic(err)
	}
	userPath := u.HomeDir
	lxrPowPath := userPath + "/.lxrpow"
	err = os.MkdirAll(lxrPowPath, os.ModePerm)
	if err != nil {
		panic(fmt.Sprintf("Could not create the directory %s", lxrPowPath))
	}
	bits := math.Log2(float64(lx.MapSize))
	filename := fmt.Sprintf(lxrPowPath+"/lxrpow-%04x-passes-%02d-bits.dat", lx.Passes, int64(bits))
	// Try and load our byte map.
	fmt.Printf("Reading ByteMap Table %s\n", filename)

	start := time.Now()
	dat, err := os.ReadFile(filename)
	// If loading fails, or it is the wrong size, generate it.  Otherwise just use it.
	fail := false
	if err != nil {
		fail = true
		fmt.Println("Error reading table data")
	}
	if len(dat) != int(lx.MapSize) {
		fail = true
		fmt.Printf("Expected table to be %d bytes, was %d bytes\n", lx.MapSize, len(dat))
	}
	lx.ByteMap = dat
	var expectedCS uint64 = 0xed0536be7833ca15
	cs := lx.check()
	if cs != expectedCS {
		fail = true
		fmt.Printf("Expected checksum %x but was %x\n", expectedCS, cs)
	}
	if fail {
		lx.GenerateTable()
		fmt.Println("Writing ByteMap Table ")
		lx.WriteTable(filename)
	}
	fmt.Printf("Finished Reading ByteMap Table. Total time taken: %s\n", time.Since(start))
}

func (lx *LxrPow) check() uint64 {
	var state uint64 = 0
	for i := 0; i < len(lx.ByteMap); i += 8 {
		a := binary.BigEndian.Uint64(lx.ByteMap[i:])
		state ^= state<<23 ^ a<<11
		state ^= state >> 3
		state ^= state << 17
	}
	return state
}

// WriteTable caches the byteMap to disk so it only has to be generated once
func (lx *LxrPow) WriteTable(filename string) {
	os.Remove(filename)

	// open output file
	fo, err := os.Create(filename)
	if err != nil {
		panic(err)
	}
	// close fo on exit and check for its returned error
	defer func() {
		if err := fo.Close(); err != nil {
			panic(err)
		}
	}()

	// write a chunk
	w := bufio.NewWriter(fo)
	bufSize := 4096 // 4KiB
	for i := 0; i < len(lx.ByteMap); i += bufSize {
		j := i + bufSize
		if j > len(lx.ByteMap) {
			j = len(lx.ByteMap)
		}
		if nn, err := w.Write(lx.ByteMap[i:j]); err != nil {
			panic(fmt.Sprintf("error writing ByteMap to disk: %d bytes written, %v", nn, err))
		}
	}
	err = w.Flush()
	if err != nil {
		panic(err)
	}
}

// GenerateTable generates the ByteMap.
// Initializes the map with an incremental sequence of bytes,
// then does P passes, shuffling each element in a deterministic manner.
func (lx *LxrPow) GenerateTable() {
	lx.ByteMap = make([]byte, int(lx.MapSize)) //
	// Our own "random" generator that really is just used to shuffle values
	MapMask := lx.MapSize - 1

	// Fill the ByteMap with bytes ranging from 0 to 255.  As long as MapSize%256 == 0, this
	// looping and masking works just fine.
	fmt.Println("Initializing the Table")
	for i := range lx.ByteMap {
		lx.ByteMap[i] = byte(i)
	}
	fmt.Printf("start cs %x\n", lx.check())
	// Now what we want to do is just mix it all up.  Take every byte in the ByteMap list, and exchange it
	// for some other byte in the ByteMap list. Note that we do this over and over, mixing and more mixing
	// the ByteMap, but maintaining the ratio of each byte value in the ByteMap list.
	fmt.Println("Shuffling the Table")
	var offset uint64 = 0x7a43b26c611e135f // Initialize the offset used by rand, an arbitrary value with no zero byte values
	var cnt uint64
	for i := uint64(0); i < lx.Passes; i++ {
		fmt.Printf("%d", i)
		for i := range lx.ByteMap {
			if i%200_000_000 == 0 { //                               Give feedback
				fmt.Print(".") //                                    A period every 200M bytes swapped
			} //
			offset ^= offset<<13 ^ cnt                            // More classical Marsaglia Xorshift RNG
			offset ^= offset >> 23                                //
			offset ^= offset << 5                                 //
			r := offset & MapMask                                 // Mask r to an index within the byteMap
			a := uint64(lx.ByteMap[r])                            // walk cnt forward by a (0-255)
			cnt += a                                              //
			lx.ByteMap[i], lx.ByteMap[r] = byte(a), lx.ByteMap[i] // Swap rth and ith bytes in ByteMap
		}
		fmt.Printf(" Index %10d Meg of %10d Meg -- Pass is %5.1f%% Complete\n",
			len(lx.ByteMap)/1024000, len(lx.ByteMap)/1024000, float64(100))
	}
	cs := lx.check()
	fmt.Printf("\nChecksum: 0x%x\n", cs)
}
