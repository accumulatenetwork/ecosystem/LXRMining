// Copyright 2023 The Accumulate Authors
//
// Use of this source code is governed by an MIT-style
// license that can be found in the LICENSE file or at
// https://opensource.org/licenses/MIT.
package pow

import (
	"crypto/sha256"
	"crypto/sha512"
	"encoding/binary"
	"fmt"
	"sync/atomic"
	"testing"
	"time"

	humanize "github.com/dustin/go-humanize"
)

// Using this test to check the performance of the LxrPow
func Test_LxrPoWSpeed(t *testing.T) {
	doLxPow := true

	lx := new(LxrPow).Init(16, 30, 6)
	type result struct{ nonce, pow uint64 }
	results := make(chan result, 100)

	oprHash := sha256.Sum256([]byte("This is a test"))
	var hashCnt int64
	nonce := uint64(time.Now().UnixNano())

	goCnt := 1
	for i := 0; i < goCnt; i++ {

		go func(instance int, a [32]byte) {
			var best uint64
			for {
				nonce = nonce<<17 ^ nonce>>9 ^ uint64(hashCnt) ^ uint64(instance) // diff nonce for each instance
				h := atomic.AddInt64(&hashCnt, 1)
				var nPow uint64
				if doLxPow {
					nPow = lx.LxrPoW(a[:], uint64(h))
				} else {
					var n [8]byte
					binary.BigEndian.PutUint64(n[:], nPow)
					x := append(a[:], n[:]...)
					shaHash := sha256.Sum256(x)
					nPow = binary.BigEndian.Uint64(shaHash[:])
				}
				if nPow > best {
					results <- result{nonce, nPow}
					if nPow > best {
						best = nPow
					}
				}
			}
		}(i, oprHash)
	}
	start := time.Now()
	time.Sleep(1 * time.Second)
	fmt.Println("Starting")
	var best result
	// pull the results, and print the best hashes
	for i := 1; i < 14; i++ {
		var r result
		select {
		case r = <-results:
			if r.pow > best.pow {
				best = r
			}
		default:
			current := time.Since(start)
			rate := float64(hashCnt) / float64(current.Nanoseconds()) * 1000000000
			fmt.Printf("      time: %10s TH: %10d H/s: %12.5f Pow: %016x\n",
				fmt.Sprintf("%3d:%02d:%02d", int(current.Hours()), int(current.Minutes())%60, int(current.Seconds())%60),
				hashCnt, rate, best.pow)
			time.Sleep(5 * time.Second)
		}

	}
}

// Using this test to check the performance of the LxrPow
func Test_Sha256PoWSpeed(t *testing.T) {
	t.Skip()
	oprHash := sha256.Sum256([]byte("This is a test"))
	var hashCnt int64
	start := time.Now()
	fmt.Println("Starting")
	// pull the results, and print the best hashes
	for {
		for i := 0; i < 100000; i++ {
			hashCnt++
			oprHash = sha256.Sum256(oprHash[:])
		}
		current := time.Since(start)
		rate := float64(hashCnt) / float64(current.Nanoseconds()) * 1000000000
		fmt.Printf("      time: %10s TH: %10d H/s: %12.5f \n",
			fmt.Sprintf("%3d:%02d:%02d", int(current.Hours()), int(current.Minutes())%60, int(current.Seconds())%60),
			hashCnt, rate)
	}
}

// Using this test to check the performance of the LxrPow
func Test_StartValue(t *testing.T) {
	lx := new(LxrPow)
	_ = lx
	Hash := sha256.Sum256([]byte{1, 2, 4, 5, 7, 3, 1})

	for i := 0; i < 10; i++ {
		Hash = sha256.Sum256(Hash[:])
		var last, d, diff, under, cnt uint64
		for i := 0; i < 100000; i++ {
			_, state := lx.mix(Hash[:], uint64(i))
			//state := binary.BigEndian.Uint64(Hash[:])
			//Hash = sha256.Sum256(Hash[:])

			cnt++
			if last > state {
				d += last - state
			} else {
				d += state - last
			}
			last = state
			d = d % (1024 * 1024 * 1024)
			if d < 1024 {
				under++
			}
			diff += d
		}
		fmt.Printf("%d %x\n", i, Hash[:10])
		fmt.Printf("average jump: %s\n", humanize.Comma(int64(diff/cnt%(1024*1024*1024))))
		fmt.Printf("Under 1k: %d \n", under)
	}
}

func Fill(b byte) (array [32]byte) {
	for i := range array {
		array[i] = b
	}
	return array
}

// TestMix
// Test the avalanche quality of the mix function.  It doesn't have to be
// as good as sha256, just not be embarrassingly bad.
func TestMix(t *testing.T) {
	Test := "mix"       // Test sha512 or mix
	numTests := 10000   // How many tests
	start := time.Now() // Start of test (we will time them)

	lx := new(LxrPow).Init(16, 30, 6) // Just to get access to mix
	var bitChanged [40 * 8]int        // How many times a bit changed in hash, state returned by mix
	var bitChanges int                // How many bits changed per bit modified
	hash := Fill(1)                   // Start with some values
	hash = sha256.Sum256(hash[:])     // Hash those values.

	evaluate := func(firstHash [32]byte, firstState uint64, newHash [32]byte, newState uint64) {
		var mFirst, mNew [40]byte
		copy(mFirst[:], firstHash[:])
		binary.BigEndian.PutUint64(mFirst[32:], firstState)
		copy(mNew[:], newHash[:])
		binary.BigEndian.PutUint64(mNew[32:], newState)

		for i := range bitChanged {
			byi := i / 8 // Byte Index
			bii := i % 8 // Bit Index
			mask := byte(1 << bii)
			if mFirst[byi]&mask != mNew[byi]&mask {
				bitChanged[i]++
				bitChanges++
			}
		}
	}

	for i := 0; i < numTests; i++ {
		nonce := uint64(i)        // Do 64 tests against a particular nonce, flipping single bits
		testNonce := nonce        // This will be the nonce we test by flipping each bit
		for i := 0; i < 64; i++ { // flip one bit, mix, and grade
			bit := i % 64 //    Bit to test

			if Test == "mix" { //    What are we testing? mix? or sha512?
				firstHash, firstState := lx.mix(hash[:], testNonce)

				testNonce ^= 1 << bit //

				newHash, newState := lx.mix(hash[:], testNonce)

				evaluate(firstHash, firstState, newHash, newState)
			} else { // If not mix, sha512.Sum384()
				var all [40]byte
				copy(all[:], hash[:])
				binary.BigEndian.PutUint64(all[32:], testNonce)
				hash := sha512.Sum384(all[:])
				var firstHash [32]byte
				copy(firstHash[:], hash[:])
				firstState := binary.BigEndian.Uint64(hash[32:])

				testNonce ^= 1 << bit
				binary.BigEndian.PutUint64(all[32:], testNonce)

				hash = sha512.Sum384(all[:])
				var newHash [32]byte
				copy(newHash[:], hash[:])
				newState := binary.BigEndian.Uint64(hash[32:])

				evaluate(firstHash, firstState, newHash, newState)

			}
		}
	}

	perfect := 1.0 / float64(len(bitChanged)) * 100
	diff := 48*8/2 - float64(bitChanges)/float64(numTests)/64
	if diff < 0 {
		diff = -diff
	}
	var min, max float64
	min = 10000
	max = -10000
	for _, b := range bitChanged {
		e := perfect - float64(b)/float64(bitChanges)*100
		if min > e {
			min = e
		}
		if max < e {
			max = e
		}
	}
	fmt.Printf("                         test: %s\n", Test)
	fmt.Printf("                         time: %v\n", time.Since(start))
	fmt.Printf("                tests per sec: %f\n", float64(numTests)*64/time.Since(start).Seconds())
	fmt.Printf("          Total Bits per test: %d\n", 48*8)
	fmt.Printf("   Goal Bits flipped per test: %d\n", 48*8/2)
	fmt.Printf("Average bits changed per test: %f +/- %f\n", float64(bitChanges)/float64(numTests)/64, diff)
	fmt.Printf("                    min entry: %4.2f%+8.5f%%\n", perfect, min)
	fmt.Printf("                    max entry: %4.2f%+8.5f%%\n", perfect, max)
	fmt.Println()
	fmt.Print("Percent error per bit:\n")
	for i, b := range bitChanged {
		e := perfect - float64(b)/float64(bitChanges)*100
		fmt.Printf("%8.5f%% ", e)
		if (i+1)%16 == 0 {
			fmt.Println()
		}
	}
	fmt.Println()
}

func TestMixC(t *testing.T) {
	lx := NewLxrPow(15, 30, 6)
	var hash [32]byte
	nh, a := lx.mix(hash[:], 0)
	nh, b := lx.mix(nh[:], 1)
	nh, c := lx.mix(nh[:], 2)
	fmt.Printf("\n%x %x %x %x\n", a, b, c, nh)
}

func TestCMain(t *testing.T) {
	fmt.Println("Load ByteMap")
	lx := NewLxrPow(15, 30, 6)
	fmt.Println("The byteMap is loaded")
	fmt.Printf("%13s %13s %10s %16s   %15s\n", //
		"total(s)", "block(s)", "nonce", "PoW", "hashes/sec") //
	var hash [32]byte           // Start with a hash of all zeros
	var hashCnt = 0             // Count hashes to compute hash/second (hps)
	for i := 0; i < 1000; i++ { //
		fmt.Printf("Now Mining: %x\n", hash) // Start mining the new hash
		var min uint64 = 0xFFFFFFFFFFFFFFFF  // set min to max unsigned long long value
		done := false                        // Done is set when a block difficulty is found
		for nonce := uint64(0); !done; {

			newHash, v := lx.LxrPoWHash(hash[:], nonce) // Get the LxrPow for current nonce
			if v < min && v < uint64(1)<<52 {           // Print new solutions < some minimum difficulty
				min = v                                  // Set as the new highest solution found
				fmt.Printf("%13.3s %13.3s %10d %016x\n", // Print out the new solution
					"",    // Total time
					"",    // Time mining this block
					nonce, // The nonce solution found
					v)     // The mining value using the nonce
			} //
			done = v < uint64(1)<<45 // Check against the difficulty for EOB
			if done {                // If at EOB
				copy(hash[:], newHash)
			}
			nonce++
			hashCnt++
		}
	}
}
