// Copyright 2023 The Accumulate Authors
//
// Use of this source code is governed by an MIT-style
// license that can be found in the LICENSE file or at
// https://opensource.org/licenses/MIT.

package pow

import (
	"encoding/binary"
	"time"
)

// LXRHash uses a combination of shifts and exclusive or's to combine inputs and states to
// create a hash with strong avalanche properties.  This means that if any one bit is changed
// in the inputs, on average half of the bits of the output change.  This quality makes an
// algorithmic attack on mining very, very unlikely.
//
// First, the hash and nonce are "mixed" such that a reasonable avalanche property is obtained.
//
// Secondly, the resulting 48 bytes are mapped through a 1 GB byteMap where random accesses have
// an even chance of producing a value between 0-255. Cycling through lookups from random bytes in
// the 1 GB space produces a resulting value to be graded.  In the example code provided, the
// lowest unsigned value is sought.
//
// The Shift XoR Random Number Generator (RNG) was invented by George Marsaglia and published
// in 2003. Please refer to the readme on the LXRHashing repository.
// https://gitlab.com/accumulatenetwork/ecosystem/LXRMining/-/blob/main/README.md
//
//   * No elements of the triplets are zero
//   * All elements are relatively prime to each other
//   * All elements Are small relative to an unsigned long (64)
//   * The right shift generally smaller than the left shifts
//
// In my testing, and according to Marsaglia, no elements of these triplets fitting this criteria
// perform better or worse. Adding constants and other outside elements help the distribution of
// values.
//
// Further, two different sponge states are used, mix() and LXRPoW()
//
// mix()
// Combines the 32 byte hash with the unsigned long nonce.  This is done as rapidly as possible
// with an avalanche property only enough to ensure repeated hashes with minimal bit changes to
// nonces do not gain an advantage during mining by tracing the same paths through the byteMap,
// making better use of the cache.
//
// LXRPoW()
// Accepts a 32 byte hash with the unsigned long nonce. The sponge is initialized using mix().
// Then Each byte is translated using a sequenced Pseudo random sequenced index into the
// 1 GB byteMap. This process is repeated for a number of loops, building up a sponge value.
// More loops makes the hash slower and more dependent on the random access speed to the
// 1 GB byteMap.  As processors grow in speed, and chips accommodate greater caches, the
// byteMap and the loop number increased.
//
// mix() is then used again to "squeeze out" the hash, where the high 8 bytes represent the
// possible PoW solution.

// rand()
// Returns a random index into the byteMap.  Note that the state in offset is updated on every call,
// so that the sequence of values produced will produce a sequence of pseudo random numbers.

type LxrPow struct {
	Loops   uint64 // The number of loops translating the ByteMap
	ByteMap []byte // Integer Offsets
	MapBits uint64 // Number of bits for the MapSize (2^MapBits = MapSize)
	MapSize uint64 // Size of the translation table (must be a power of 2)
	Passes  uint64 // Passes to generate the rand table
}

// NewLxrPow
//
// Return a new instance of the LxrPow work function
// Loops  - the number of passes through the hash made through the hash itself.  More
//
//	loops will slow down the hash function.
//
// Bits   - number of bits used to create the ByteMap. 30 bits creates a 1 GB ByteMap
// Passes - Number of shuffles used to randomize the ByteMap. 6 seems sufficient
//
// Any change to Loops, Bits, or Passes will map the PoW to a completely different
// space.
func NewLxrPow(Loops, Bits, Passes uint64) *LxrPow {
	lx := new(LxrPow)
	lx.Init(Loops, Bits, Passes)
	return lx
}

// LxrPoW() returns a 64 byte value indicating the proof of work of a hash
// This is designed to allow the use of any hash function, but make the grading
// of the proof of work dependent on the random byte access limits of LXRHash.
//
// The bigger uint64, the more PoW it represents.  The first byte is the
// number of leading bytes of FF, followed by the leading "non FF" bytes of the pow.
func (lx LxrPow) LxrPoW(hash []byte, nonce uint64) (pow uint64) {
	_, state := lx.LxrPoWHash(hash, nonce)
	return state
}

// Same as LxrPoW, but returns the underlying hash as well
func (lx LxrPow) LxrPoWHash(hash []byte, nonce uint64) (newHash []byte, pow uint64) {
	mask := lx.MapSize - 1

	LHash, state := lx.mix(hash, nonce)

	// Make the specified "loops" through the LHash.  This is 40 bytes; 32 from the sha256 and
	// 8 bytes from the trailing part of the hash.  Keeping or not keeping the trailing 8 only
	// changes how many translations per loop are done.
	for i := uint64(0); i < lx.Loops; i++ {
		for j := range LHash {
			b := uint64(lx.ByteMap[state&mask]) // Hit the ByteMap
			v := uint64(LHash[j])               // Get the value from LHash as a uint64
			state ^= state<<17 ^ b<<31          // A modified XorShift RNG
			state ^= state>>7 ^ v<<23           // Fold in a random byte from
			state ^= state << 5                 // the byteMap and the byte
			LHash[j] = byte(state)              // from the hash.
		}
		time.Sleep(0)
	}
	_, state = lx.mix(LHash[:], state)
	return LHash[:], state // Return the pow of the sha256 of the translated hash
}

// mix
// takes the nonce and the hash and mixes the bits of both into a 40 byte string
// such that changing one bit pretty much changes the odds of changing any bit
// by 50% ish.  In other words, mix stands in for a cryptographic hash, speeding
// up the time it takes to get to translating through the ByteMap.
func (lx LxrPow) mix(hash []byte, nonce uint64) (newHash [32]byte, state uint64) {
	if len(hash) != 32 {
		panic("must provide a 32 byte hash")
	}
	state = 0xb32c25d16e362f32 // Set the state to an arbitrary unsigned long with bits
	nonce ^= nonce<<19 ^ state // set and not set in each byte.  Run the nonce through a
	nonce ^= nonce >> 1        // Xorshift RNG
	nonce ^= nonce << 3

	var array [5]uint64      // Array of uint64, the nonce and 4 longs holding hash
	array[0] = nonce         // This array along with the state is the sponge for the LXRHash
	for i := 1; i < 5; i++ { // Walk through the last 4 elements of array
		array[i] = binary.BigEndian.Uint64(hash[(i-1)<<3:]) // Get 8 bytes of the hash as array entry
		state ^= state<<7 ^ array[i]                        // shift state combine the whole hash into
		state ^= state >> 3                                 // the state while randomizing the state
		state ^= state << 5                                 // through a Xorshift RNG.
	}

	// Mix what is effectively sponge of 5 elements and a state

	for i, a := range array {
		left1 := (a & 0x1F) | 1                // Use the element to pick 3 random shifts for the Xorshift RNG
		right := ((a & 0xF0) >> 5) | 1         // Note that the right is smaller than the left
		left2 := ((a&0x1F0000)>>17)<<1 ^ left1 // And xor-ing left1 with left2 ensures they are different
		state ^= state<<left1 ^ a<<5           // Randomize the state and array element
		state ^= state >> right                // using the random shifts
		state ^= state << left2                //
		array[i] ^= state                      // apply to the array
	}

	// Extract the 32 byte hash and mix the state a bit more
	for i, a := range array[:4] {
		a ^= state
		binary.BigEndian.PutUint64(newHash[i*8:], a)
		state ^= state<<27 ^ uint64(i)<<19 // Mix the state a bit more, fold in the counter
		state ^= state >> 11               //
		state ^= state << 13               //
	}

	return newHash, state
}
