// Copyright 2023 The Accumulate Authors
//
// Use of this source code is governed by an MIT-style
// license that can be found in the LICENSE file or at
// https://opensource.org/licenses/MIT.
package pow

import (
	"fmt"
	"testing"
	"time"
)

func TestLxrPow_Init(t *testing.T) {
	t.Skip()
	for i := 8; i < 31; i++ {
		p := new(LxrPow)
		p.Init(100, uint64(i), 6)

		var Sums [256]int
		for _, b := range p.ByteMap {
			Sums[b]++
		}
		v := 0
		for _, cnt := range Sums {
			if v != cnt {
				v = cnt
				fmt.Printf("ByteMap size %16d Bits %2d %02x\n", len(p.ByteMap), i, cnt)
			}
		}
		fmt.Println()
		time.Sleep(0)
	}
}

func TestGenerateTable(t *testing.T) {
	start := time.Now()
	NewLxrPow(3, 26, 6)
	fmt.Printf("Test took %v", time.Since(start))
}
