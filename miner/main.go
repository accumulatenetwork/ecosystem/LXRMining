// Copyright 2023 The Accumulate Authors
//
// Use of this source code is governed by an MIT-style
// license that can be found in the LICENSE file or at
// https://opensource.org/licenses/MIT.
package main

import (
	//"fmt"
	//"os"
	"fmt"
	"os"
	"os/signal"
	"time"

	"github.com/pegnet/LXRMining/accumulate"
	"github.com/pegnet/LXRMining/cfg"
	mine "github.com/pegnet/LXRMining/minerInstance"
	"github.com/pegnet/LXRMining/sim"
	"github.com/pegnet/LXRMining/validator"
)

// Create a simulated miner network
// Validators
//   - Provide settings to the miners
//   - includes the hash for the miners to use for PoW
//   - Grade miner submissions
//   - Tabulate points for miners for each block
//   - Provide payments to miners
//
// Miners
//   - Work on PoW on hash picked up from settings
func main() {
	c := cfg.NewConfig() // pull the parameters from the commandline

	var validatorList []*validator.Validator   // built a set of validators
	for i := 0; i < int(c.ValidatorCnt); i++ { // Just running one validator for now
		v, err := validator.NewValidator(sim.GetURL()+"/book", c.LX)
		if err != nil {
			panic(err)
		}
		accumulate.MiningADI.Reports[0].Settings.ValidatorIndex = v.UrlIndex
		accumulate.MiningADI.RegisterValidator(v.URL)
		validatorList = append(validatorList, v)
	}

	for _, v := range validatorList { // Start all the validators
		go v.Start() //
	}

	minerList := make(map[string]*mine.Miner) // Create the list of miners
	for j := uint64(0); j < c.MinerCnt; j++ {
		m := new(mine.Miner)
		if err := m.Init(c); err != nil { // If init fails, noting to do but fail
			panic(err)
		}
		c.TokenURL = sim.GetURL() + "/tokens" // GetURL ensures a random unique URL
		minerList[m.Cfg.TokenURL] = m
		go m.Run()
		c = c.Clone()
	}

	// For grins, we use an Interrupt Handler to shut everything down
	AddInterruptHandler(func() {
		fmt.Print("<Break>\n")
		fmt.Print("Gracefully shutting down the mining simulation...\n")
		for _, m := range minerList {
			m.Stop()
		}
		fmt.Print("Waiting...\r\n")
		time.Sleep(time.Second / 5)
		os.Exit(0)
	})

	for {
		time.Sleep(time.Second)
	}

}

// interruptChannel is used to receive SIGINT (Ctrl+C) signals.
var interruptChannel chan os.Signal

// addHandlerChannel is used to add an interrupt handler to the list of handlers
// to be invoked on SIGINT (Ctrl+C) signals.
var addHandlerChannel = make(chan func())

// mainInterruptHandler listens for SIGINT (Ctrl+C) signals on the
// interruptChannel and invokes the registered interruptCallbacks accordingly.
// It also listens for callback registration.  It must be run as a goroutine.
func mainInterruptHandler() {
	// interruptCallbacks is a list of callbacks to invoke when a
	// SIGINT (Ctrl+C) is received.
	var interruptCallbacks []func()

	// isShutdown is a flag which is used to indicate whether or not
	// the shutdown signal has already been received and hence any future
	// attempts to add a new interrupt handler should invoke them
	// immediately.
	var isShutdown bool

	for {
		select {
		case <-interruptChannel:
			// Ignore more than one shutdown signal.
			if isShutdown {
				fmt.Println("Ctrl+C Already being processed!")
				continue
			}
			isShutdown = true
			fmt.Println("Received SIGINT (Ctrl+C).  Shutting down...")

			// Run handlers in LIFO order.
			for i := range interruptCallbacks {
				idx := len(interruptCallbacks) - 1 - i
				callback := interruptCallbacks[idx]
				callback()
			}

		case handler := <-addHandlerChannel:
			// The shutdown signal has already been received, so
			// just invoke and new handlers immediately.
			if isShutdown {
				handler()
			}

			interruptCallbacks = append(interruptCallbacks, handler)
		}
	}
}

// AddInterruptHandler adds a handler to call when a SIGINT (Ctrl+C) is
// received.
func AddInterruptHandler(handler func()) {
	// Create the channel and start the main interrupt handler which invokes
	// all other callbacks and exits if not already done.
	if interruptChannel == nil {
		interruptChannel = make(chan os.Signal, 1)
		signal.Notify(interruptChannel, os.Interrupt)
		go mainInterruptHandler()
	}

	addHandlerChannel <- handler
}
