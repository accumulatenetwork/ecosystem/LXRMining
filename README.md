# LXRMining
This project is designed to provide retrospective mining to Accumulate.  Retrospective 
mining allows miners to effectively "sign" with Proof of Work (PoW) the state of a protocol.

PoW is criticized for the custom and/or expensive hardware required to be profitable, as well as for the power requirements of such hardware.  However, algorithms exist that do limit how much expensive hardware can improve mining, and how much power such hardware can consume.  

Every computer relies on memory caches for speed.  And speed involves high power requirements.  If a hashing algorithm depends on speed of DRAM access, then fast processor speed isn't terribly important.  Algorithms that do random DRAM access as part of their hash cannot be enhanced in a cost effective fashion by custom hardware because DRAMs cannot be "on chip" with processors and ASICs.  This is a limit due to the physical size of the memories and the constraints memory density imposes. 

With this in mind, a CPU based algorithm needs a very low computational component, combined with a maximized memory access component. 

For the computational component we use an shift/xor hashing algorithm based on George Marsaglia's work.  One of his papers can be found here: https://www.jstatsoft.org/article/view/v008i14

Note that Random Number Generators (RNGs) are mathematical twins with of a sort with hashing algorithms.  Mathematically speaking, the input of digital data to a hash function deterministically, where a set of input data always produces a certain fixed random result.  Feed that random number back into a hash function, and a series of random numbers can be generated in series.  Good cryptographic hash functions would have very long periods before they generate a hash that matches some past hash.

The PegNet used the LXRHash (a predecessor to the LXRPoW hash used in this repository) as its mining hash, where PoW was evaluated by the highest unsigned value (or lowest unsigned value).  The PegNet has been down for a year or more (for reasons not related to PoW), but will be relaunched in a few months.  Accumulate is looking to add the LXRPoW as a mechanism for retrospective mining.

Some background on XOR and Shift XOR hashes is useful. XOR based hashes are used in memory bank distributions. The math behind them and discussions in how they are used can be found in the literature, including:

* https://github.com/pgroupATusc/XOR-hash
* https://ieeexplore.ieee.org/document/7271051
* https://www.jstatsoft.org/article/view/v008i14

The basic theory for memory constrained CPU mining is pretty simple. We want to use Sha256 for cryptographic hashes, but would like to mine in a way that blows the memory caches maintained by CPUs. We don't care if mining is cryptographically secure (i.e. might produce duplicate solutions for different inputs) as long as the memory intensive process must be done to find a solution, and to evaluate the solution.  For more detail you can read the detail on the patent for the technique:

* https://patents.justia.com/patent/20220103341
* https://patents.justia.com/patent/20230147204

## Validator Coordination
The retrospective mining for Accumulate needs a set of validators.  These validators evaluate the hash power of submissions by miners.  The Validators are signers responsible for the Mining.Acme ADI

### Mining.acme
The following accounts are maintained by the miners in the Mining.acme ADI:
* **mining.acme/book/1**           -- Control book for mining validators; this is controlled by the staking signers
* **mining.acme/book/2**           -- Holds the ordered list of validator signing books.
                                      To be a validator, a node must have a key in this book
* **mining.acme/submissions**      -- Data account that holds submissions in its scratch chain 
                                      for miners submitting their solutions to Mining.  This is 
                                      an open data account that allows anyone to write to
* **mining.acme/Settings**         -- Data account holding settings to be used by miners to mine Accumulate
* **mining.acme/URLRegistry**      -- Data account holding the token accounts of miners, key books of 
                                      validators, and staking accounts of validators so that these urls 
                                      can be referenced by indexes
* **mining.acme/StakingRegistry**  -- List of Staking accounts managed by mining validators. Holds transactions
                                      involving staking accounts, including adding staking accounts, removal of 
                                      staking accounts, slashing of staking accounts, withdraws from staking
                                      accounts, etc.
* **mining.acme/PointsAwarded**    -- Entries include a record for points added to Miners, and a payout records
                            
* **mining.acme/Totals**           -- Point Balance for all miners whose balances changed by a payout record
                                      that details payouts to each miner (whose points are additionally cleared)
* **mining.acme/PoW**              -- List of winning PoW solutions
* **mining.acme/ValidatorResults** -- Entries include Result Hash Records (RHRs) for all running validators, possible
                                      rejection records from validators that find RHRs that don't agree with their
                                      calculations. Third parties can do the same calculations and record their
                                      rejection records for RHRs on the mining.acme/Disputes chain.
                                      if they run the mining app, and can submit disputes
* **mining.acme/Disputes**         -- Dispute records against ValidatorHashes or any other issue in mining operation
                                      Anyone can submit a Dispute Record
* **mining.acme/Work**             -- In the initial deployment, miners will earn "Work" tokens. 

## Operation

### Tokenomics
Initially we will issue **mining.acme/Work** tokens.  In the future we may issue ACME directly, either as a share
of the protocol inflation, or from a budget of ACME allocated from protocol grants or community contributions.

Tokenomics of Work tokens:
* 42 million token supply limit
* Payouts every 4 hours
* Inflation set at an APR of 16% of the unissued supply per year
* Budget is recalculated at every payout
* ((TokenLimit - TotalSupply) * PercentInflation) / numberOfPayoutsPerYear
    * TokenLimit = 42 million
    * TotalSupply = Work Tokens issued - Work Tokens burned
    * PercentInflation = 16 percent
    * numberOfPayoutsPerYear = 365.25 * 24 / 4 = 2191.5

### Initiation
All of the accounts and mining.acme have to be set up.  The validators have to be set up with their key books
and the key books funded with credits.  The validator key books are added to the second page of mining.acme/book.

The initial settings record is added to Settings

The validators all start up their validator nodes

### Validator node initiation
All the PointsAwarded records from the beginning of the data chain are processed to create the current Totals
for mining. The latest Settings record is loaded. The current Totals index must match the Settings index.

### Validator processing
1. All validators process submissions until 300 submissions cross the difficulty threshold.  
2. The Points to be added to miners are calculated
3. The rewards to be paid to the set of winning miners are calculated
4. The list of miners to have their points cleared is calculated
5. A hash of all calculations from steps 2-4 is added to each validator's index then hashed to create a 
   Result Hash
6. A Result Hash Record is created using the hash from 5. appended to the Validator index and hashed
7. All RHRs are evaluated, and any that do not validate, create and add a rejection record
8. Continue processing 7. until 2/3 of the needed RHRs are recorded and validated
9. If the validator has the lowest hash in its RHR:
   1. Submit the new Settings Record to start the next round of mining
   2. If on a payout index:
      1. Submit the Points Awarded record
      2. Submit the Payout/Cleared record
      2. Submit the payout transaction