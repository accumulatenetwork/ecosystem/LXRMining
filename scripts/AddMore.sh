
# Routing:
# debug account route <network, i.e. mainnet or kermit/v2> adiname.acme

fct=acc://b6a007c8b08a86e022631704c8a8fe50802e66c5702c3fbf/ACME # a random FCT address lite account
base=$(cat adibase.dat)

. ./distribution.sh

while :
do
  for adi in "${sortedADIs[@]}"
  do
    for src in $lite 
    do
      sleep 15m
      date
      ./kermit tx create $src $adi/tokens .0001
      echo $src $adi
    done
  done
done
