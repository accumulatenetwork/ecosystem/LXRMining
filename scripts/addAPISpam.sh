
base=$(cat adibase.dat)

. ./distribution.sh

numADIs=${#ADIs[@]}
id=${RANDOM:0:3}
k=$id
while true
do    	
        (( k++ ))
        i=$(echo $numADIs $k | gawk '{ srand($2); print int($1 * rand()) }')
        (( k++ ))
        j=$(echo $numADIs $k | gawk '{ srand($2); print int($1 * rand()) }')

        if (( i == j )); then
            continue
        fi

        source=${ADIs[$i]}
        rs=${routes[$i]}
        
        chico=$(echo $rs $rd | gawk '{a=0} /Chico/ {a=1} {print a}')
        if (( chico == 0 )); then
            continue
        fi

        d=$(date | gawk '{printf("%s %s %s %s %s",$1,$2,$3,$4,$5)}')

        echo $rs $source
        b=$(./kermit get $source 2>&1)
        echo $d $b
        b=$(./kermit get $source/book 2>&1)
        echo $d $b
        b=$(./kermit get $source/book/1 2>&1)
        echo $d $b
        b=$(./kermit get $source/tokens 2>&1)
        echo $d $b
        b=$(./kermit get $source/x 2>&1)
        echo $d $b
        
done
