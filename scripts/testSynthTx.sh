#!/bin/bash

# Configuration
Yutu=testtest.acme/trades
Apollo=TheApolloADI.acme/trades
Chandrayaan=ChandrayaanADI-a.acme/trades


YutuKey=MHz126KPgHUXYG4TBVboZLJGKh7rGV5Rn8K4ENB256Poc6QomJ7rgHh
ApolloKey=MHz126nwMwaAZ4b8LqPyTXrv6P8rPNLyVWAUvByZLKumwzdbocChtvx
ChandrayaanKey=MHz126nwMwaAZ4b8LqPyTXrv6P8rPNLyVWAUvByZLKumwzdbocChtvx

# Counter file
COUNTER_FILE="tradeCnt.dat"

# Load counter from file, initialize to 0 if file doesn't exist
if [ -f "$COUNTER_FILE" ]; then
    counter=$(<"$COUNTER_FILE")
else
    counter=0
fi

echo "Starting with counter: $counter"

# Array of ADIs
ADIs=("$Yutu" "$Apollo" "$Chandrayaan")

# For each ADI as focus
for focus in "${ADIs[@]}"; do
    echo "Focus ADI: $focus"
    
    # Have other ADIs write to focus
    for writer in "${ADIs[@]}"; do
        if [ "$writer" != "$focus" ]; then
            echo "  $writer writing to $focus"
            data="{\"counter\":$counter,\"writer\":\"$writer\"}"
            accumulate data write "$focus" "$data" 
        fi
    done
done

# Increment and save counter
((counter++))
echo $counter > "$COUNTER_FILE"
echo "Ending with counter: $counter"

