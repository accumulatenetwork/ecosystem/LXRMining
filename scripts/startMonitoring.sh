#!/bin/bash

# Terminal window dimensions and positioning
TERM_W=120   # Width in characters
TERM_H=5     # Height for main process terminals
LOG_H=12     # Height for log terminals
POS_X=50     # X position from left edge
POS_Y=50     # Y position from top edge
WORKDIR=~/go/src/gitlab.com/AccumulateNetwork/Ecosystem/LXRMining/scripts

# Launch main process terminals (smaller height)
GEOM="${TERM_W}x${TERM_H}+${POS_X}+${POS_Y}"
gnome-terminal --geometry="$GEOM" -- bash -c 'cd "$WORKDIR" && ./monitor.sh         >> monitor.log; exec bash'
gnome-terminal --geometry="$GEOM" -- bash -c 'cd "$WORKDIR" && ./pubBitcoinPrice.sh >> publish.log; exec bash'
gnome-terminal --geometry="$GEOM" -- bash -c 'cd "$WORKDIR" && ./moveTokens.sh      >> move.log; exec bash'
gnome-terminal --geometry="$GEOM" -- bash -c 'cd "$WORKDIR" && ./faucetLoad.sh      >> faucet.log; exec bash'
gnome-terminal --geometry="$GEOM" -- bash -c 'cd "$WORKDIR" && ./touchAll.sh  2>&1  >> touch.log; exec bash'

# Launch log monitoring terminals (taller)
LOG_GEOM="${TERM_W}x${LOG_H}+${POS_X}+${POS_Y}"
gnome-terminal --geometry="$LOG_GEOM" -- bash -c 'cd "$WORKDIR" && tail -f monitor.log; exec bash'
gnome-terminal --geometry="$LOG_GEOM" -- bash -c 'cd "$WORKDIR" && tail -f publish.log; exec bash'
gnome-terminal --geometry="$LOG_GEOM" -- bash -c 'cd "$WORKDIR" && tail -f move.log; exec bash'
gnome-terminal --geometry="$LOG_GEOM" -- bash -c 'cd "$WORKDIR" && tail -f faucet.log; exec bash'
gnome-terminal --geometry="$LOG_GEOM" -- bash -c 'cd "$WORKDIR" && tail -f touch.log 2>&1 | gawk "/[0-9]/{print}"; exec bash'


