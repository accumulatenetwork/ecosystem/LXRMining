#!/bin/bash

Yutu=testtest1.acme
Chandrayaan=testtest2.acme
Apollo=testtest5.acme

# Array of signing books using the ADI variables
declare -a SIGNING_BOOKS=(
    "$Yutu/book_2/1"
    "$Chandrayaan/book_2/1"
    "$Apollo/book_2/1"
)

# Parallel array of friendly names
declare -a FRIENDLY_NAMES=(
    "Yutu"
    "Chandrayaan"
    "Apollo"
)
current_book=0

sleeptime=10

write_and_verify() {
    local account=$1
    local value=$2
    local signing_book=$3
    local start_time=$(date +%s.%N)
    
    # Write data with signing book
    testAccumulate data write "$account" "$value" --scratch --sign-for "$signing_book" >&2 2>> touch.log

    while true; do
        # Store full response for debugging
        local full_response=$(testAccumulate data get "$account")
        
        # Add error handling and default to 0 if parsing fails
        local read_ascii=$(echo "$full_response" | jq -r '.data.entry.data[0]' || echo "0")
        
        # Ensure we have a valid number
        if [[ "$read_ascii" =~ ^[0-9]+$ ]]; then
            local read_value=$read_ascii
            if [ "$read_value" -eq "$value" ]; then
                local end_time=$(date +%s.%N)
                local elapsed=$(echo "$end_time - $start_time" | bc)
                echo "$elapsed:$signing_book"
                return 0
            fi
        fi
        
        sleep 1
    done
}

cnt=999

while true; do
    ((cnt++))
    
    # Get current signing book before each write
    signing_book=${SIGNING_BOOKS[$current_book]}
    
    # Get timing and signer info
    IFS=: read -r time_yutu yutu_signer <<< "$(write_and_verify "$Yutu/txs" "$cnt" "$signing_book")"
    IFS=: read -r time_chandrayaan chandrayaan_signer <<< "$(write_and_verify "$Chandrayaan/txs" "$cnt" "$signing_book")"
    IFS=: read -r time_apollo apollo_signer <<< "$(write_and_verify "$Apollo/txs" "$cnt" "$signing_book")"

    # Get friendly name directly from the parallel array
    current_signer=${FRIENDLY_NAMES[$current_book]}


    # Increment book counter after all writes
    current_book=$(( (current_book + 1) % ${#SIGNING_BOOKS[@]} ))

    
    printf "%s | signer: %-12s | Yutu: %6.2f | Chandrayaan: %6.2f | Apollo: %6.2f |\n" \
        "$(date '+%m-%d %H:%M')" "$current_signer" "$time_yutu" "$time_chandrayaan" "$time_apollo"
    
    sleep $sleeptime
done