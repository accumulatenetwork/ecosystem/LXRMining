#!/bin/bash

echo pubBitcoinPrice.sh

# Constants
WORKDIR=~/go/src/gitlab.com/AccumulateNetwork/Ecosystem/LXRMining/scripts
PRICE_URL="https://api.coinbase.com/v2/prices/BTC-USD/spot"
SLEEP_TIME=60
PRICE_ACCOUNT=testtest.acme/bitcoin-price

# Ensure we're in the correct directory
cd "$WORKDIR" || exit 1

# Main loop
while true; do
    # Get current timestamp in UTC
    timestamp=$(date -u '+%a %b %-d %H:%M:%S PM UTC %Y')
    
    # Fetch Bitcoin price from Coinbase
    price=$(curl -s "$PRICE_URL" | jq -r '.data.amount' | xargs printf "%.10f")
    
    # Print status with timestamp
    printf "%s BTC Price: $%s\n" "$timestamp" "$price"
    
    # Write to Accumulate with the correct format
    testAccumulate data write $PRICE_ACCOUNT "bitcoin" "$price" "USD" "$timestamp" --scratch --sign-with testtest.acme/book
    
    # Sleep for one minute
    sleep $SLEEP_TIME
done
