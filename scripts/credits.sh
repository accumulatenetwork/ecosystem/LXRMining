
# Routing:
# debug account route <network, i.e. mainnet or kermit/v2> adiname.acme

base=$(cat adibase.dat)

. ./distribution.sh

./kermit credits $lite $lite2 1000000
./kermit credits $lite $lite5 1000000

for adi in "${ADIs[@]}"
do

  ./kermit credits $lite $adi/book/1 1000000

done
