
# Routing:
# debug account route <network, i.e. mainnet or kermit/v2> adiname.acme

. distribution.sh
base=$(cat adibase.dat)

balance=$(./kermit get $lite | gawk '/[^t]Balance/{print int($3)}')
echo $balance at $lite



for base in "${ADIs[@]}"
do
   balance1=$(./kermit get $base/tokens | gawk '/[^t]Balance/{print int($3)}')
   echo $base/tokens $balance1
done

# ./kermit adi create $lite windstorm$i.acme a
# ./kermit credits $lite windstorm$i.acme/book/1 1000000
# ./kermit get rainstorm$i.acme/book/1
# ./kermit account create token windstorm$i.acme windstorm$i.acme/tokens acme
# ./kermit tx create $lite windstorm$i.acme/tokens 60

#balance=$(./kermit get p$i.acme/tokens | gawk '/Balance/{gsub(".00000000",""); print $3}')
#send=$((balance-10))
#echo x0 $balance $send
#./kermit tx create x.acme/tokens prices.acme/tokens $send

# ./kermit credits prices.acme/tokens p$i.acme/book/1 100000

# ./kermit adi create prices.acme p$i.acme a
# ./kermit account create token p$i.acme p$i.acme/tokens acme
# ./kermit tx create prices.acme/tokens p$i.acme/tokens 2500

