
# Routing:
# debug account route <network, i.e. mainnet or kermit/v2> adiname.acme

. liteAccounts.sh

base=$(cat adibase.dat)
cnt=$(cat adicnt.dat)

credits=$(./kermit get $lite | gawk '/Credit/{printf "%d", $3}')
if [ "$credits" ] && (( $credits < 10000)) 
then
  ./kermit credits $lite $lite 1000000
fi
credits=$(./kermit get $lite2 | gawk '/Credit/{printf "%d", $3}')
if [ "$credits" ] && (( $credits < 10000)) 
then
  ./kermit credits $lite $lite2 1000000
else
  echo fail
fi
credits=$(./kermit get $lite5 | gawk '/Credit/{printf "%d", $3}')
if [ "$credits" ] && (( $credits < 10000)) 
then
  ./kermit credits $lite $lite5 1000000
else
  echo fail
fi

      
ADIs=( )
routes=( )
echo ${parts[*]}
idx=0
for part in  "Harpo" "Chico" "Groucho"
do 
    s=0
    for (( i = 0 ; i < $cnt ; i++ ));
    do
       for ((j=s  ; j < 100000 ; j++ ))
       do
           (( s++ )) 
           route=$(debug account route kermit $base$j.acme | gawk '/Routes/{print $3}' ); 
           if [ "$part" == "$route" ] 
           then
              ADIs+=("$base$j.acme")
              routes+=("$route")
              echo Index $idx  $route "$base$j.acme"  
              (( idx++ ))
              break
           fi
       done
    done          
done
IFS=$'\n' sortedADIs=($(sort -n <<<"${ADIs[*]}"))
for value in "${sortedADIs[@]}"; do
  route=$(debug account route kermit $value | gawk '/Routes/{print $3}' ); 
  echo "$value" "$route"
done
