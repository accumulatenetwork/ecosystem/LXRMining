#!/bin/bash

# Source the lite accounts
source liteAccounts.sh

# Debug: Print the lite account variables
echo "Lite accounts:"
echo "lite: $lite"
echo "lite2: $lite2"
echo "lite5: $lite5"

# Constants
WORKDIR=~/go/src/gitlab.com/AccumulateNetwork/Ecosystem/LXRMining/scripts
SLEEP_TIME=1

check_balance() {
    local full_account=$1
    # Convert ACME to acme in the account path
    local account=${full_account//ACME/acme}
    local start_time=$(date +%s.%N)
    local attempts=0
    local max_attempts=2
    
    # Get initial balance and convert from fixpoint
    local initial_raw=$(./kermit get "$account" --json | jq -r '.data.balance' || echo "0")
    local initial_balance=$(echo "scale=8; $initial_raw / 100000000" | bc)
    
    # Request tokens from faucet
    ./kermit faucet "$account/tokens" >&2 2>> faucet.log
    
    # Monitor balance changes
    while [ $attempts -lt $max_attempts ]; do
        local current_raw=$(./kermit get "$account" --json | jq -r '.data.balance' || echo "0")
        local current_balance=$(echo "scale=8; $current_raw / 100000000" | bc)
        if [ "$current_raw" != "$initial_raw" ]; then
            local end_time=$(date +%s.%N)
            local elapsed=$(echo "$end_time - $start_time" | bc)
            echo "$elapsed"
            return 0
        fi
        ((attempts++))
        sleep 1
    done
    
    # If we get here, we've exceeded max attempts
    echo "stall"
    return 1
}

# Ensure we're in the correct directory
cd "$WORKDIR" || exit 1

# Main loop
while true; do
    # Get current timestamp
    timestamp=$(date '+%Y-%m-%d %H:%M:%S')
    
    # Check each lite account
    time_lite=$(check_balance "$lite")
    time_lite2=$(check_balance "$lite2")
    time_lite5=$(check_balance "$lite5")
    
    # Print status with timestamp and times
    printf "%s | Harpo: %6s | Groucho: %6s | Chico: %6s\n" \
        "$timestamp" "$time_lite" "$time_lite2" "$time_lite5"
    
    # Sleep before next round
    sleep $SLEEP_TIME
done