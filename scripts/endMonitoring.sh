#!/bin/bash

# Kill all the monitoring processes first
pkill -f "monitor.sh"
pkill -f "pubBitcoinPrice.sh"
pkill -f "moveTokens.sh"
pkill -f "faucetLoad.sh"
pkill -f "touchAll.sh"

# Kill the tail processes watching the logs
pkill -f "tail -f monitor.log"
pkill -f "tail -f publish.log"
pkill -f "tail -f move.log"
pkill -f "tail -f faucet.log"
pkill -f "tail -f touch.log"

# Optional: Kill any remaining gnome-terminal instances from our script
# pkill -f "gnome-terminal.*scripts"

echo "All monitoring processes and terminals have been terminated." 