#!/bin/bash

# Configuration
STAT_FILE="stat.dat"
SLEEP_DURATION="30m"
RETRY_SLEEP="1m"

# Network components
declare -A COMPONENTS=(
    ["DN"]="kompendium.acme,12"
    ["Apollo"]="kompendium.acme,14"
    ["Chandrayaan"]="TurtleBoat.acme,16"
    ["Yutu"]="tfa.acme,18"
)

# Initialize old and last reported values
for component in "${!COMPONENTS[@]}"; do
    eval "${component}_old=0"
    eval "${component}_last_reported=0"
done

# Logging function
log() {
    echo "$(date): $1" | tee -a network_status.log
}

# Fetch network heights
fetch_heights() {
    if ! debug network status mainnet 2>/dev/null > "$STAT_FILE"; then
        log "Error: Failed to fetch network status"
        return 1
    fi

    for component in "${!COMPONENTS[@]}"; do
        IFS=',' read -r pattern field <<< "${COMPONENTS[$component]}"
        if ! height=$(gawk "/$pattern/ {printf(\"%d\",substr(\$$field,3))}" "$STAT_FILE"); then
            log "Error: Failed to parse height for $component"
            return 1
        fi
        eval "${component}_new=$height"
    done
}

# Check for changes
check_changes() {
    local changed=0
    for component in "${!COMPONENTS[@]}"; do
        local old_val="${component}_old"
        local new_val="${component}_new"
        if [ "${!old_val}" = "${!new_val}" ] && [ -n "${!new_val}" ]; then
            log "FAIL $component: ${!old_val} - ${!new_val}"
            gawk "/${COMPONENTS[$component]%%,*}/ {print \"$component fail \" \$0}" "$STAT_FILE"
            changed=1
        fi
    done
    return $changed
}

# Update values
update_values() {
    for component in "${!COMPONENTS[@]}"; do
        eval "${component}_old=\$${component}_new"
    done
}

print_status() {
    local stalled_components=()
    for component in "${!COMPONENTS[@]}"; do
        local new_val="${component}_new"
        local last_reported="${component}_last_reported"
        local diff=$((${!new_val} - ${!last_reported}))
        case "$component" in
            "DN")
                log "dn:   ${!new_val} ($diff)"
                ;;
            "Apollo")
                log "bvn0: ${!new_val} ($diff)"
                ;;
            "Yutu")
                log "bvn1: ${!new_val} ($diff)"
                ;;
            "Chandrayaan")
                log "bvn2: ${!new_val} ($diff)"
                ;;
        esac
        # Check if component has stalled
        if [ $diff -eq 0 ]; then
            stalled_components+=("$component")
        fi
        # Update last reported value
        eval "${component}_last_reported=${!new_val}"
    done
    # Print stalled components if any
    if [ ${#stalled_components[@]} -gt 0 ]; then
        log "WARNING: The following components have stalled:"
        for stalled in "${stalled_components[@]}"; do
            log "  - $stalled"
        done
    else
       log "All Okay!"
    fi
}

# Cleanup function
cleanup() {
    log "Exiting..."
    rm -f "$STAT_FILE"
    exit 0
}

# Set up trap
trap cleanup SIGINT SIGTERM

# Main loop
log "Starting network status check"

# Get initial status
if ! fetch_heights; then
    log "Error: Failed to get initial status"
    exit 1
fi
print_status
update_values  # Initialize old values

while true; do
    sleep "$SLEEP_DURATION"
    
    log "-"
    log "Checking network status..."
    if ! fetch_heights; then
        continue
    fi
    
    if check_changes; then
        print_status
        sleep "$RETRY_SLEEP"
        cat "$STAT_FILE" >> network_status.log
    else
        print_status
        update_values
    fi
done