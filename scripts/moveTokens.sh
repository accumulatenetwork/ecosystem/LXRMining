#!/bin/bash

# Set and verify working directory
WORKDIR=~/go/src/gitlab.com/AccumulateNetwork/Ecosystem/LXRMining/scripts
cd "$WORKDIR" || exit 1

# Load configuration
base=$(cat adibase.dat)
. ./distribution.sh

# Initialize variables
numADIs=${#ADIs[@]}
id=${RANDOM:0:3}
k=$id

# Main token movement loop
while true; do
    # Select random source and destination
    ((k++))
    i=$(echo $numADIs $k | gawk '{ srand($2); print int($1 * rand()) }')
    ((k++))
    j=$(echo $numADIs $k | gawk '{ srand($2); print int($1 * rand()) }')

    # Skip if same source and destination
    if (( i == j )); then
        continue
    fi

    # Set source and destination addresses
    source=${ADIs[$i]}
    dest=${ADIs[$j]}
    rs=${routes[$i]}
    rd=${routes[$j]}

    # Get current timestamp
    d=$(date | gawk '{printf("%s %s %s %s %s",$1,$2,$3,$4,$5)}')

    # Check balances
    b=$(./kermit get $source/tokens 2>&1)
    balance=$(echo $b | gawk '/Balance/{printf ("%9.5f", $11)}')

    b2=$(./kermit get $dest/tokens 2>&1)
    balance2=$(echo $b2 | gawk '/Balance/{printf ("%9.5f", $11)}')

    # Log transaction attempt
    echo $d source: $i $rs $source -$b- dest: $j $rd $dest -$b2- >> err.dat

    # Verify balances are valid
    if [ "$balance" ] && [ "$balance2" ] && (( $(echo "$balance >= 1" | bc -l) )); then
        echo "GOOD" >> err.dat
    else
        echo -n $rs $balance "."
        sleep 30s
        echo ID: $id "SKIP " $rs $source "("$balance")" "send to" $rd $dest "(" $balance2 ")" >> err.dat
        continue
    fi

    # Swap source/dest if dest has more tokens
    if (( $(echo "$balance < $balance2" | bc -l) )); then
        b=$balance2
        balance2=$balance
        balance=$b
        s=$source
        source=$dest
        dest=$s
        r=$rs
        rs=$rd
        rd=$r
    fi

    # Perform token transfer
    amount=1
    d=$(date | gawk '{printf("%s %s %s %s %s",$1,$2,$3,$4,$5)}')
    fmt="\"ID: %3d   %3d (%4s) %2d => %2s (%4s)   %-4s - %s %8s => %8s\\n\""
    err=$(./kermit --debug tx create $source/tokens $dest/tokens $amount 2>> err.dat | gawk 'BEGIN{e= "ERR"}/Transaction/{e="ok"}END{print e}')
    
    # Log results
    echo "$d" >> err.dat
    echo $i $j $amount >> err.dat
    echo | gawk "{printf($fmt,$id,$i,\"$balance\", $amount,$j, \"$balance2\",\"$err\",\"$d\",\"$rs\",\"$rd\")}"

    # Handle errors
    if [[ "$err" = "ERR" ]]; then
        sleep 30s
    fi

    # Wait before next iteration
    sleep 5m
done
