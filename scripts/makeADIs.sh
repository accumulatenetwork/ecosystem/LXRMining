
# Routing:
# debug account route <network, i.e. mainnet or kermit/v2> adiname.acme

. ./distribution.sh

./kermit credits $lite $lite 1000000
./kermit credits $lite2 $lite 1000000
./kermit credits $lite5 $lite 1000000

# Create all the ADIs
for base in "${ADIs[@]}" 
do
  echo create $base
  ./kermit adi create $lite $base a 
done

# Give credits to ADIs
for base in "${ADIs[@]}" 
do
  echo 1000000 credits to $base/book/1
  ./kermit credits $lite $base/book/1 1000000 
done

# Create data and token accounts
for base in "${ADIs[@]}" 
do
  echo create $base/tokens
  ./kermit account create token $base $base/tokens acme 
  ./kermit account create data $base $base/data
done

# Fund Token accounts
for base in "${ADIs[@]}" 
do
  echo send 50 acme to $base/tokens
   ./kermit tx create $lite $base/tokens 50 
done



