// Copyright 2023 The Accumulate Authors
//
// Use of this source code is governed by an MIT-style
// license that can be found in the LICENSE file or at
// https://opensource.org/licenses/MIT.
package validator

import (
	"fmt"
	"time"

	"github.com/pegnet/LXRMining/accumulate"
	"github.com/pegnet/LXRMining/pow"
)

type Validator struct {
	URL             string                      // The Validator's URL
	UrlIndex        uint64                      // Index of this validator
	LX              *pow.LxrPow                 // A pointer to the proof of work struct (1 GB)
	LastBlockTime   float64                     // Time it took to build the last block in seconds
	CurrentWinners  []accumulate.Submission     // List of currentWinners
	SubmissionIndex uint64                      // Index of the last submission read
	BlockTimes      []float64                   // Last few block times
	OldDiff         uint64                      // A working value
	NewPoints       map[uint64]accumulate.Point // Points to be added (prior to validation)
	MinerPoints     map[uint64]accumulate.Point // Points earned by miners
	FirstIndex      uint64                      // Index of the first submission of the current block
}

// NewValidator
func NewValidator(url string, lx *pow.LxrPow) (validator *Validator, err error) {
	v := new(Validator) // could wait to ensure no error, but the code panics on error, so meh!
	v.URL = url
	if v.UrlIndex, err = accumulate.MiningADI.Url2Index(v.URL); err != nil {
		return nil, err
	}
	v.LX = lx
	v.MinerPoints = make(map[uint64]accumulate.Point)
	accumulate.MiningADI.RegisterValidator(url)
	return v, nil
}

// Start
// Updates the settings on Accumulate
func (v *Validator) Start() {

	report := accumulate.MiningADI.Sync()
	for {

		time.Sleep(time.Second / 100) // ToDo: simulates blocks in accumulate; replace with integration

		newReport := v.AddWinners(&report.Settings)
		if newReport != nil {
			fmt.Printf("URL: %20s BlockHeight %d\n", v.URL, report.Settings.BlockHeight)
			if v.UrlIndex == report.Settings.ValidatorIndex {
				v.PrintReport(report.Settings, newReport.Settings, false, false)
				accumulate.MiningADI.AddReport(*newReport)
			}
			report = *newReport 
		}

	}
}
