// Copyright 2023 The Accumulate Authors
//
// Use of this source code is governed by an MIT-style
// license that can be found in the LICENSE file or at
// https://opensource.org/licenses/MIT.
package validator

import (
	"crypto/sha256"
	"encoding/binary"
	"fmt"
	"sort"

	"github.com/pegnet/LXRMining/accumulate"
)

// ValidateWinner
// Returns true if we have an end of block, and we have 300 winners!
func (v *Validator) AddWinners(settings *accumulate.Settings) *accumulate.Report {
	var subs []accumulate.Submission
	subs, v.SubmissionIndex = accumulate.MiningADI.GetSubmissions(settings, uint64(v.SubmissionIndex))
	for _, sub := range subs {
		if len(v.CurrentWinners) == 300 { //                     End of block?
			break
		}
		// Note that GetSubmissions already validates against settings, so sub can be trusted
		v.CurrentWinners = append(v.CurrentWinners, sub) //  Add to currentWinners
	}

	if len(v.CurrentWinners) < 300 {
		return nil
	}

	report := new(accumulate.Report)
	v.DistributePoints()
	newSettings := v.ComputeNewSettings(settings)
	report.Settings = *newSettings
	if settings.BlockHeight != 0 && settings.BlockHeight%settings.PayoutFreq == 0 {
		outputs := v.PayMiners(settings)
		report.Payouts = outputs
	}
	return report
}

// DistributePoints()
// Once a block has been completed, sort the submissions, distribute points
func (v *Validator) DistributePoints() {

	//sort.Slice(v.CurrentWinners,
	//		func(i, j int) bool {
	//			return v.CurrentWinners[i].PoW > v.CurrentWinners[j].PoW
	//		})

	// Note that awarding points DOES allow duplicates
	for _, s := range v.CurrentWinners {
		p := v.MinerPoints[s.MinerIdx] // Works if MinerIdx exists or not
		p.MinerIdx = s.MinerIdx        // p.MinerIdx might not be set, so set it
		p.Points += 600                // Give all nodes that made the threshold points
		v.MinerPoints[s.MinerIdx] = p  // Update the map of miners earning points
	}

}

// ComputeNewSettings
// Calculate various values that should now be in the settings object
func (v *Validator) ComputeNewSettings(settings *accumulate.Settings) *accumulate.Settings {
	newSettings := settings.Clone()

	t, di, dh := accumulate.MiningADI.NextDNHash()                     // Get the next Directory Block Hash
	copy(newSettings.DNHash[:], dh)                                    // Copy the Directory Block Hash into newSettings
	newSettings.TimeStamp = t                                          // Copy other stuff we get.
	newSettings.DNIndex = di                                           //
	newSettings.BlockHeight++                                          // Note this is the block height for mining, not accumulate
	newSettings.SubmissionHeight = accumulate.MiningADI.SubmissionHt() // Get the current submission height from the miners

	validatorIndex := binary.BigEndian.Uint64(dh)            // compute the next leader from the dn hash
	numValidators := accumulate.MiningADI.NumberValidators() // Get the number of validators
	validatorIndex = validatorIndex % numValidators          // Mod by the number of validators
	if numValidators > 0 {                                   // If we have more than one validator
		var ndh [32]byte                                // Create a place to hold a hash
		copy(ndh[:], dh)                                // start with the dirictory block hash
		for settings.ValidatorIndex == validatorIndex { // If the index is the same as last time,
			ndh = sha256.Sum256(ndh[:])                      // hash the hash
			validatorIndex = binary.BigEndian.Uint64(ndh[:]) // and select a new validator
			validatorIndex = validatorIndex % numValidators
		}
	}
	validatorURL := accumulate.MiningADI.GetValidator(validatorIndex) // Get the validator URL
	idx, err := accumulate.MiningADI.Url2Index(validatorURL)          // Get the validator index
	if err == nil {                                                   // Note on error, stay with current validator
		newSettings.ValidatorIndex = idx // This should always be the case
	}
	LastBlockTime, newDiff, windowBlockIndex := v.AdjustDifficulty(settings, newSettings) // Adjust the index
	v.LastBlockTime = LastBlockTime                                                       // Track block times
	newSettings.WindowBlockIndex = uint64(windowBlockIndex)                               // and the WindowBlockIndex

	if newDiff != newSettings.Difficulty { // If the difficulty changes
		v.OldDiff = newSettings.Difficulty // Track the past difficulty
		newSettings.Difficulty = newDiff   // and the new one
	}

	newSettings.LastBlockTime = uint64(LastBlockTime)
	newSettings.SubmissionCnt = v.SubmissionIndex - v.FirstIndex

	sort.Slice(v.CurrentWinners, func(i, j int) bool {
		return v.CurrentWinners[i].PoW > v.CurrentWinners[j].PoW
	})

	v.CurrentWinners = v.CurrentWinners[:0] // Clear current winners
	v.FirstIndex = v.SubmissionIndex

	return newSettings
}

func (v *Validator) PrintReport(settings, newSettings accumulate.Settings, printPoints, printPayments bool) {
	// ============= Print report =============
	// Returns a printable time given seconds
	// Print function to print all points that the system is tracking.
	prt := func() { //
		var pts []accumulate.Point // Get a list of miner/points
		for _, v := range v.MinerPoints {
			pts = append(pts, v)
		}
		sort.Slice(pts, func(i, j int) bool { // Sort descending by points
			return pts[i].Points > pts[j].Points
		})

		for i, pt := range pts {
			if i%16 == 0 {
				fmt.Println()
			}
			fmt.Printf("[%4d]=%7d ", pt.MinerIdx, pt.Points)
		}
		fmt.Println()

		//	for i := range pts {
		//		fmt.Printf("%4d %s\n", i, accumulate.MiningADI.Index2Url(uint64(i)))
		//	}
		//	fmt.Println()
	}

	prtTime := func(secs float64) string {
		hours := int(secs) / 60 / 60
		minutes := int(secs/60 - float64(hours*60))
		seconds := secs - float64(hours*60*60) - float64(minutes*60)
		switch {
		case hours > 0:
			return fmt.Sprintf("%d:%02d:%04.1f", hours, minutes, seconds)
		case minutes > 0:
			return fmt.Sprintf("%02d:%04.1f", minutes, seconds)
		default:
			return fmt.Sprintf("%04.1f", seconds)
		}
	}

	if printPoints {
		prt()
	}

	cnt := 0.0
	out := "=================================================\n"
	out += fmt.Sprintf("@@%05.0f          Block#= %d\n", cnt, newSettings.BlockHeight-1) // BlockHeight is next block
	out += fmt.Sprintf("@@%05.0f       Validator= %s\n", cnt, v.URL)
	out += fmt.Sprintf("@@%05.0f TargetBlockTime= %s\n", cnt, prtTime(float64(settings.BlockTime)))
	out += fmt.Sprintf("@@%05.0f      BlockTime = %s\n", cnt, prtTime(v.LastBlockTime))
	out += fmt.Sprintf("@@%05.0f      Difficulty= %016x\n", cnt, newSettings.Difficulty)
	out += fmt.Sprintf("@@%05.0f   Previous Diff= %016x\n", cnt, v.OldDiff)
	out += fmt.Sprintf("@@%05.0f    #submissions= %d\n", cnt, newSettings.SubmissionCnt)

	fmt.Print(out)
}

// AdjustDifficulty
// Returns the time it took to produce the last hash in seconds, and the new difficulty
// if an adjustment is required.
func (v *Validator) AdjustDifficulty(settings, newSettings *accumulate.Settings) (
	lastBlockTime float64, difficulty uint64, WindowBlockIndex uint16) {

	lastBlockTime = newSettings.TimeStamp.Sub(settings.TimeStamp).Seconds()
	v.BlockTimes = append(v.BlockTimes, lastBlockTime)
	if settings.BlockHeight%settings.DiffWindow == 0 {

		var sum float64
		for _, v := range v.BlockTimes {
			sum += v
		}
		avgBlkTime := sum / float64(len(v.BlockTimes))

		// Correct by an adjusted Percentage error, to avoid over correction
		PercentError := (float64(settings.BlockTime) - avgBlkTime) / float64(settings.BlockTime) / 1.25

		// Limit the correction, since correction can tend to be over done
		if PercentError > .5 {
			PercentError = .5
		}
		if PercentError < -.5 {
			PercentError = -.5
		}

		// The calculation of a new difficulty is done in signed floating point, which
		// means the difficulty is really the lowest negative number.  Should follow
		// Bitcoin and make Difficulty the lowest number, but Meh.
		fDifficulty := float64(int64(settings.Difficulty)) // Consider difficulty signed for these calculations
		nd := fDifficulty * (1 - PercentError)             // Adjust by the calculated error
		v.BlockTimes = v.BlockTimes[:0]                    // Clear the list of block times

		return lastBlockTime, uint64(nd), uint16(settings.BlockHeight + 1)
	}
	return lastBlockTime, settings.Difficulty, uint16(settings.WindowBlockIndex)
}

// PayMiners
func (v *Validator) PayMiners(settings *accumulate.Settings) (outputs []accumulate.TxOutput) {

	var pts []accumulate.Point // Get a list of miner/points
	for _, pt := range v.MinerPoints {
		pts = append(pts, pt)
	}
	sort.Slice(pts, func(i, j int) bool { // Sort descending by points
		return pts[i].Points > pts[j].Points
	})

	// 1X + 2X + ... + len(pts) X = Budget
	// (( len(pts)+1)^2/2) X = Budget
	// X = Budget / ((len(pts)+1)*(len(pts)+1)/2)

	if len(pts) > 100 { // Only take the top 50 or less
		pts = pts[:100]
	}

	var Even float64 = .50 * settings.Budget

	lp := float64(len(pts))
	X := (settings.Budget - Even) / ((lp + 1) * (lp + 1) / 2)
	diff := settings.Budget - X*(lp+1)*(lp+1)/2 - Even
	if diff > .001 || diff < -.001 {
		panic("math failed")
	}

	for i, pt := range pts {
		tx := accumulate.TxOutput{MinerIndex: pt.MinerIdx, Amount: float64(i+1)*X + Even/lp}
		outputs = append(outputs, tx)

		p := v.MinerPoints[pt.MinerIdx] // Clear the points of the winners
		p.Points = 0
		v.MinerPoints[pt.MinerIdx] = p
	}

	// Everyone that didn't win points does get decremented a point.
	for k, pt := range v.MinerPoints {
		if pt.Points > 0 {
			pt.Points--
			v.MinerPoints[k] = pt
		}
	}

	return outputs
}
