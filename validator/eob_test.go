// Copyright 2023 The Accumulate Authors
//
// Use of this source code is governed by an MIT-style
// license that can be found in the LICENSE file or at
// https://opensource.org/licenses/MIT.
package validator

import (
	"testing"
)

func TestValidator_PayMiners(t *testing.T) {
	var Budget float64 = 1000 // total budget for payout
	var lp float64 = 50       // Number of nodes
	var Even = .30 * Budget   // Distribute 30% evenly

	X := (Budget - Even) / ((lp + 1) * (lp + 1) / 2)
	diff := Budget - X*(lp+1)*(lp+1)/2 - Even
	if diff > .001 || diff < -.001 {
		panic("math failed")
	}
}
