package live

import (
	"bytes"
	"fmt"
	"io"
	"net/http"
	"os"
)
type Coin struct {
	Code   string `json:"code"`
	Rate   float64 `json:"rate"`
	Volume int64   `json:"volume"`
	Cap    int64   `json:"cap"`
	Delta  struct {
		Hour    float64 `json:"hour"`
		Day     float64 `json:"day"`
		Week    float64 `json:"week"`
		Month   float64 `json:"month"`
		Quarter float64 `json:"quarter"`
		Year    float64 `json:"year"`
	} `json:"delta"`
}

// ApiResponse represents the entire API response
type ApiResponse []Coin

func GetAppKey(file string) string {
	return "b6674754-dc4c-40c0-a2d9-63bc453c4676"
}

func try() {
	/*
		  curl -verbose -X POST 'https://api.livecoinwatch.com/coins/list'  \
			-H 'content-type: application/json'  \
			-H "x-api-key: b6674754-dc4c-40c0-a2d9-63bc453c4676" \
			-d '{"currency":"USD","sort":"rank","order":"ascending","offset":0,"limit":1,"meta":false}' | jq
	*/
	appKey := GetAppKey("appkey_livecoinwatch.dat")
	url := "https://api.livecoinwatch.com/coins/list"

	// Create a JSON payload
	payload := []byte(`{"currency":"USD","sort":"rank","order":"ascending","offset":0,"limit":100000,"meta":false}`)

	// Create an HTTP client
	client := &http.Client{}

	// Create an HTTP request with method, URL, and payload
	req, err := http.NewRequest("POST", url, bytes.NewBuffer(payload))
	if err != nil {
		fmt.Println("Error creating request:", err)
		return
	}

	req.Header.Set("x-api-key", appKey)
	req.Header.Set("Content-Type", "application/json")

	res, err := client.Do(req)
	if err != nil {
		fmt.Println(err)
		return
	}
	defer res.Body.Close()

	body, err := io.ReadAll(res.Body) //ioutil.ReadAll(res.Body)
	if err != nil {
		fmt.Println(err)
		return
	}
	f, err := os.Create("/home/paul/tmp/prices.dat")
	if err != nil {
		panic(fmt.Sprintf("fail to open %v", err))
	}
	defer f.Close()
	_, err = f.WriteString(string(body))
	if err != nil {
		panic(fmt.Sprintf("fail to open %v", err))
	}

	fmt.Println(string(body))
}
