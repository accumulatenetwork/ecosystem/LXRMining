package mine

import (
	"crypto/sha256"
	"encoding/binary"
	"time"

	"github.com/pegnet/LXRMining/accumulate"
	"github.com/pegnet/LXRMining/cfg"
	"github.com/pegnet/LXRMining/hashing"
)

type Miner struct {
	Cfg        *cfg.Config              // Configuration settings
	HasherSet  *hashing.HasherSet       // Points to the Hasher Set
	Started    bool                     // True if the miner has been started.  Maybe not necessary
	Solutions  chan hashing.PoWSolution // Solutions are fed to the miner here
	Control    chan string              // Sending a message to this channel stops the miner
	MinerIndex uint64                   // An index value of this miner
	HashCounts map[int]uint64           // Count the hashes per miner
	Listen     chan accumulate.Report   // Track submission account by block (about 1 per second)
}

// Init
// Initialize a Miner.
func (m *Miner) Init(cfg *cfg.Config) (err error) {
	m.Cfg = cfg

	m.HashCounts = make(map[int]uint64, 1)                             // Count of total hashes
	m.Control = make(chan string, 1)                                   // The Hasher stops when told on this channel
	m.Solutions = make(chan hashing.PoWSolution, 1000)                 // Solutions are written to this channel
	m.Listen = make(chan accumulate.Report, 10)                        // Register to get settings as they are created
	accumulate.MiningADI.AddListener(m.Listen)                         // Add the listener
	hash := sha256.Sum256([]byte(cfg.TokenURL))                        // calculate a unique hash for each token url
	nonce := binary.BigEndian.Uint64(hash[:])                          // take the top 8 bytes as a nonce
	nonce ^= uint64(time.Now().UnixNano())                             // Add some "time" to the nonce
	m.HasherSet = hashing.NewHashers(cfg.Instances, nonce, cfg.LX)     // Allocate the Hashers
	m.HasherSet.SetSolutions(m.Solutions)                              // Override their Solutions channel
	m.MinerIndex, err = accumulate.MiningADI.Url2Index(m.Cfg.TokenURL) // Register Miner with the protocol
	if err != nil {
		return err
	}
	return nil
}

func (m *Miner) Stop() {
	m.HasherSet.Stop()
	m.Control <- "stop"
}

// Run
// The job of the miner is to find the best hash it can from its hashers
// When hashers find a solution, those are fed to WriteSolution.  WriteSolution
// implements strategies for optimal submission of mining records
func (m *Miner) Run() {
	if m.Started {
		return
	}
	m.Started = true
	m.HasherSet.Start() // Start up the hashers
	report := accumulate.MiningADI.Sync()
	m.Listen <- report
	settings := report.Settings
	for {
		select {
		case solution := <-m.Solutions: // New solutions have to be graded
			if len(m.Listen) > 0 {
				for len(m.Listen) > 0 {
					report = <-m.Listen
				}
				settings = report.Settings
				for len(m.Solutions) > 0 { // Toss any pending solutions
					<-m.Solutions
				}
				break
			}
			if solution.PoW > settings.Difficulty && solution.DNHash == settings.DNHash {
				m.HashCounts[int(solution.Instance)] = solution.HashCnt // Collect all the hashing counts from hashers

				solution.TokenURL = m.Cfg.TokenURL // Save the TokenURL
				submission := new(accumulate.Submission)
				submission.TimeStamp = time.Now()
				submission.SubmissionHeight = settings.SubmissionHeight
				submission.DNHash = settings.DNHash
				submission.DNIndex = settings.DNIndex
				submission.MinerIdx = m.MinerIndex
				submission.Nonce = solution.Nonce
				submission.PoW = solution.PoW

				accumulate.MiningADI.AddSubmission(*submission)
			}

		case <-m.Control:
			m.Stop()
		case report = <-m.Listen:
			for len(m.Listen) > 0 {
				report = <-m.Listen
			}
			settings = report.Settings
			for len(m.Solutions) > 0 { // Toss any pending solutions
				<-m.Solutions
			}
			// Update all the hashers with the new Hash
			m.HasherSet.BlockHashes <- hashing.Hash{DNHash: settings.DNHash, Difficulty: settings.Difficulty}
		}
	}
}
