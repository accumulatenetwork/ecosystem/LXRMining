package mine

import (
	"fmt"
	"testing"
)

func TestDiffLimit_AdjustLimit(t *testing.T) {
	var x uint64 = 0xFFFF000000000000

	y := uint64(float64(int64(x)) * 1.30)
	fmt.Printf("%x %x\n", x, y)
}
