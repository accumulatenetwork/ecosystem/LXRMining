// For now, query prices and post a oracle record
package integration

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/go-kit/kit/auth/jwt"
)

// Define the top 30 crypto symbols
var cryptoSymbols = []string{"BTC", "ETH", "BNB", "XRP", "USDT", "ADA", "DOT", "UNI", "LTC", "LINK", "BCH", "XLM", "THETA", "FIL", "USDC", "TRX", "DOGE", "WBTC", "VET", "SOL", "EOS", "KLAY", "BTT", "MIOTA", "CRO", "BSV", "XMR", "NEO", "ATOM", "HT"}

// Define the top important indices symbols
var indicesSymbols = []string{"SPX", "DJI", "NDX", "RUT", "VIX", "GSPC", "IXIC", "NYA", "XAX", "RUI", "RUA", "MID", "N225", "FTSE", "GDAXI", "FCHI", "HSI", "SSE", "BSESN", "TWII"}

// Define the LiveCoinWatch API key
var liveCoinWatchAPIKey = "YOUR_API_KEY"

// Define the Barchart OnDemand API key
var barchartOnDemandAPIKey = "YOUR_API_KEY"

// Define the accumulate URL and token
var accumulateURL = "https://api.accumulate.network"
var accumulateToken = "YOUR_TOKEN"

// Define the JWT secret key
var jwtSecretKey = "YOUR_SECRET_KEY"

func main() {
	http.HandleFunc("/home", handlePage)
	err := http.ListenAndServe(":8080", nil)
	if err != nil {
		fmt.Println("There was an error listening on port :8080", err)
	}
}

func handlePage(writer http.ResponseWriter, request *http.Request) {
	writer.Header().Set("Content-Type", "application/json")

	// Validate the JWT token from the request header
	tokenString := request.Header.Get("Authorization")
	if tokenString == "" {
		http.Error(writer, "Missing token", http.StatusUnauthorized)
		return
	}
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		return []byte(jwtSecretKey), nil
	})
	if err != nil {
		http.Error(writer, "Invalid token", http.StatusUnauthorized)
		return
	}
	if !token.Valid {
		http.Error(writer, "Invalid token", http.StatusUnauthorized)
		return
	}

	// Create a new OPR record
	opr := opr.NewOraclePriceRecord()

	// Set the PegNet assets to the crypto and indices symbols
	opr.SetPegAssets(append(cryptoSymbols, indicesSymbols...))

	// Fetch the prices for the crypto symbols from the LiveCoinWatch API
	cryptoPrices, err := fetchCryptoPrices(opr.GetPegAssets())
	if err != nil {
		http.Error(writer, "Error fetching crypto prices", http.StatusInternalServerError)
		return
	}

	// Fetch the prices for the indices symbols from the Barchart OnDemand API
	indicesPrices, err := fetchIndicesPrices(opr.GetPegAssets())
	if err != nil {
		http.Error(writer, "Error fetching indices prices", http.StatusInternalServerError)
		return
	}

	// Merge the crypto and indices prices into one map
	prices := make(map[string]float64)
	for k, v := range cryptoPrices {
		prices[k] = v
	}
	for k, v := range indicesPrices {
		prices[k] = v
	}

	// Set the prices to the OPR record
	opr.SetPegPrices(prices)

	// Print the OPR record
	fmt.Println(opr)
}

// fetchCryptoPrices is a function that fetches the prices for the crypto symbols from the LiveCoinWatch API
func fetchCryptoPrices(symbols []string) (map[string]float64, error) {
	// Create a map to store the prices
	prices := make(map[string]float64)

	// Create a HTTP client
	client := &http.Client{}

	// Loop through the symbols
	for _, symbol := range symbols {
		// Skip the non-crypto symbols
		if !isCrypto(symbol) {
			continue
		}

		// Build the API URL with the symbol and the API key
		url := fmt.Sprintf("https://api.livecoinwatch.com/coins/single?currency=USD&code=%s&meta=true&key=%s", symbol, liveCoinWatchAPIKey)

		// Create a new HTTP request
		req, err := http.NewRequest("GET", url, nil)
		if err != nil {
			return nil, err
		}

		// Set the accumulate token in the request header
		req.Header.Set("X-Accumulate-Token", accumulateToken)

		// Send the request and get the response
		resp, err := client.Do(req)
		if err != nil {
			return nil, err
		}
		defer resp.Body.Close()

		// Decode the JSON response
		var data map[string]interface{}
		err = json.NewDecoder(resp.Body).Decode(&data)
		if err != nil {
			return nil, err
		}

		// Get the price from the response
		price, ok := data["rate"].(float64)
		if !ok {
			return nil, fmt.Errorf("invalid price for %s", symbol)
		}

		// Store the price in the map
		prices[symbol] = price
	}

	// Return the prices map
	return prices, nil
}

// fetchIndicesPrices is a function that fetches the prices for the indices symbols from the Barchart OnDemand API
func fetchIndicesPrices(symbols []string) (map[string]float64, error) {
	// Create a map to store the prices
	prices := make(map[string]float64)

	// Create a HTTP client
	client := &http.Client{}

	// Build the API URL with the symbols and the API key
	url := fmt.Sprintf("https://marketdata.websol.barchart.com/getQuote.json?apikey=%s&symbols=%s", barchartOnDemandAPIKey, joinSymbols(symbols))

	// Create a new HTTP request
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, err
	}

	// Set the accumulate token in the request header
	req.Header.Set("X-Accumulate-Token", accumulateToken)

	// Send the request and get the response
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	// Decode the JSON response
	var data map[string]interface{}
	err = json.NewDecoder(resp.Body).Decode(&data)
	if err != nil {
		return nil, err
	}

	// Get the results from the response
	results, ok := data["results"].([]interface{})
	if !ok {
		return nil, fmt.Errorf("invalid results")
	}

	// Loop through the results
	for _, result := range results {
		// Cast the result to a map
		resultMap, ok := result.(map[string]interface{})
		if !ok {
			return nil, fmt.Errorf("invalid result")
		}

		// Get the symbol and the price from the result
		symbol, ok := resultMap["symbol"].(string)
		if !ok {
			return nil, fmt.Errorf("invalid symbol")
		}
		price, ok := resultMap["lastPrice"].(float64)
		if !ok {
			return nil, fmt.Errorf("invalid price for %s", symbol)
		}

		// Store the price in the map
		prices[symbol] = price
	}

	// Return the prices map
	return prices, nil
}

// isCrypto is a helper function that checks if a symbol is a crypto symbol
func isCrypto(symbol string) bool {
	for _, s := range cryptoSymbols {
		if s == symbol {
			return true
		}
	}
	return false
}

// joinSymbols is a helper function that joins the symbols with a comma
func joinSymbols(symbols []string) string {
	result := ""
	for i, symbol := range symbols {
		result += symbol
		if i < len(symbols) - 1 {
			result += ","
		}
	}
	return result
}